import unittest

from main.hardware.cpu import CPU
from mock import Mock
from main.interruptions.interruption_manager import InterruptionManager
from main.strings.instruction import INSTRUCTION
from main.system.dispatcher import Dispatcher
from main.system.log.output import Output
from main.interruptions.handler import Handler

class TestCpu(unittest.TestCase):
    
    def setUp(self):
        
        self.end_instruction    = INSTRUCTION.END
        self.io_instruction     = INSTRUCTION.IO 
    
#     def test_cpu_call_memory_fetch_on_process(self):
#         memory                          = Mock()
#         memory.get.return_value         = 'io'
#         interruption_manager            = Mock()
#         output                          = Output()
#         cpu                             = CPU(memory, interruption_manager, output)
#         cpu.pc                          = 0
#         cpu.base_address                = 0
#         
#         cpu.process()
#         
#         memory.put.assert_called_once_with(0)
    
    def test_cpu_call_end_instruction_on_evaluate(self):
        
        handler                     = Handler()
        dispatcher                  = Dispatcher()
        int_manager                 = InterruptionManager(dispatcher)
        int_manager.set_handler(handler)
        output                      = Output()
        cpu                         = CPU(None, int_manager, output)
        cpu.instruction             = self.end_instruction
        
        cpu.evaluate()
        #cpu.output.print_log()
        
        self.assertEquals(int_manager.irqCPU.irq, self.end_instruction)
        
    def test_cpu_call_io_instruction_on_evaluate(self):
        handler         = Handler()
        dispatcher      = Dispatcher()
        int_manager     = InterruptionManager(dispatcher)
        int_manager.set_handler(handler)
        output          = Output()
        cpu             = CPU(None, int_manager, output)
        cpu.instruction = self.io_instruction
        
        cpu.evaluate()
        #cpu.output.print_log()
        
        self.assertEquals(int_manager.irqCPU.irq, self.io_instruction)
        