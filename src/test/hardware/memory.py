import unittest

from main.hardware.memory.memory import Memory
from mock import Mock
from main.strings.instruction import INSTRUCTION

class TestMemory(unittest.TestCase):
    
    def test_put(self):

        memory = Memory()
        
        instruction = INSTRUCTION.END
        
        memory.put(instruction, 0)
        
        self.assertEquals(memory.get(0), instruction)
        