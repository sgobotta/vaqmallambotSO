'''
Created on 7 de jun. de 2016

@author: gabriel
'''
import unittest
from mock import Mock


from main.hardware.memory.dynamic_relocation.relocation_register import RelocationRegister
from main.hardware.memory.dynamic_relocation.block import Block
from main.hardware.memory.memory_manager import MemoryManager

class Test(unittest.TestCase):


    def test_assign_to_block(self):
  
        relocation_register = RelocationRegister(None, 10)
        program = [1,2,3,4,5]
        block1 = Mock()
        block2 = Mock()
        pcb = Mock()
        mi_array = [block1, block2]
        relocation_register.memory_manager = Mock()
        
        relocation_register.set_free(mi_array)
        
        block1.get_size.return_value = 4
        block2.get_size.return_value = 6
        block1.get_base_address.return_value = 4
        block2.get_base_address.return_value = 5
        pcb.get_pid.return_value = 1
        
        relocation_register.assign_to_block(program, pcb, block2)
        
        expected    = block2
        actual      = relocation_register.get_used()[0]
        
        self.assertEquals(expected, actual)


    def test_compact(self):
        memory_manager = Mock()
        relocation_register = RelocationRegister(memory_manager, 10)
        block1              = Block(0,4)
        block2              = Block(8,10)
        used1               = Block(5,8)        
         
         
        relocation_register.add_used_block(used1)
        relocation_register.add_free_block(block1)
        relocation_register.add_free_block(block2)
         
        relocation_register.compact()
         
         
        self.assertEqual(len(relocation_register.get_free_blocks()), 1)
         
     
    def test_merge_free_blocks_whit_contiguous_blocks(self):
        memory_manager          = Mock()
        relocation_register     = RelocationRegister(memory_manager, 10)
        block1                  = Block(0,3)
        block2                  = Block(4,10)
        free_list               = [block1, block2]
        relocation_register.set_free(free_list)
         
        relocation_register.merge_free_blocks()
        
        free_list               = relocation_register.get_free_blocks()
        result_block            = free_list.pop()
        free_list.append(result_block)
         
        self.assertEquals(result_block.get_base_address(), 0)
        self.assertEquals(result_block.get_end(), 10)
        self.assertEquals(len(free_list), 1)
        
    
    def test_merge_free_bocks_whitout_contiguous_blocks(self):
        memory_manager          = Mock()
        relocation_register     = RelocationRegister(memory_manager, 10)
        block1                  = Block(0,2)
        block2                  = Block(3,5)
        block3                  = Block(7,10)
        free_list               = [block1, block2, block3]
        relocation_register.set_free(free_list)
         
        relocation_register.merge_free_blocks()
        
        free_list               = relocation_register.get_free_blocks()
        result_block            = free_list.pop()
        free_list.append(result_block)
         
        self.assertEquals(result_block.get_base_address(), 0)
        self.assertEquals(result_block.get_end(), 5)
        self.assertEquals(len(free_list), 2)        
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()