'''
Created on Jun 2, 2016

@author: santiago
'''
import unittest
from main.hardware.memory.memory_assignation_policy.best_fit import BestFit
from mock import Mock

class TestBestFit(unittest.TestCase):

    def test_best_fit_returns_a_potential_register_on_evaluate(self):
        
            algorithm = BestFit()
            
            current_register   = Mock()
            potential_register = Mock()
            
            potential_register.get_size.return_value = 10
            current_register.get_size.return_value = 14
            
            program_size       = 10
            algorithm.best_offset = program_size
            
            algorithm.evaluate_blocks(current_register, potential_register, program_size)
            
            self.assertEqual(algorithm.best_register, potential_register)

    def test_best_fit_returns_the_most_suitable_block_on_search_free_block(self):
        
        relocation_register = Mock()
        
        algorithm = BestFit(relocation_register)
        
        program                     = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
        
        blocks = []
        register0 = Mock()
        register1 = Mock()
        register2 = Mock()
        register3 = Mock()
        register4 = Mock()
        register5 = Mock()
        register6 = Mock()
        register7 = Mock()
        register8 = Mock()
        register9 = Mock()
        
        register0.get_size.return_value = 14
        register1.get_size.return_value = 10
        register2.get_size.return_value = 16
        register3.get_size.return_value = 19
        register4.get_size.return_value = 25
        register5.get_size.return_value = 100
        register6.get_size.return_value = 15
        register7.get_size.return_value = 7
        register8.get_size.return_value = 1
        register9.get_size.return_value = 4
        
        blocks.append(register0)
        blocks.append(register1)
        blocks.append(register2)
        blocks.append(register3)
        blocks.append(register4)
        blocks.append(register5)
        blocks.append(register6)
        blocks.append(register7)
        blocks.append(register8)
        blocks.append(register9)
        
        relocation_register.get_free_blocks.return_value = blocks
        
        best_register = algorithm.search_free_block(program)
        
        self.assertEquals(best_register, register6)