'''
Created on 8/6/2016

@author: leonardo
'''
import unittest
from main.hardware.memory.memory_assignation_policy.worst_fit import WorstFit
from mock import Mock

class TestWorstFit(unittest.TestCase):
    
    def test_search_free_block_returns_the_free_block(self):
        
        relocation_register = Mock()
        worst_fit           = WorstFit(relocation_register)
        program             = [1,2,3]
        block1              = Mock()
        block2              = Mock()
        block3              = Mock()
        free_list           = [block1, block2, block3]
        
        relocation_register.get_free_blocks.return_value = free_list
        block1.get_size.return_value                         = 8
        block2.get_size.return_value                         = 5
        block3.get_size.return_value                         = 10
        
        actual = worst_fit.search_free_block(program)
        
        self.assertEquals(block3, actual)