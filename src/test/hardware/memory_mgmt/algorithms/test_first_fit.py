'''
Created on 2 de jun. de 2016

@author: gabriel
'''
import unittest
from main.hardware.memory.memory_assignation_policy.first_fit import FirstFit

from mock import Mock


class TestFirstFit(unittest.TestCase):

    def test_search_free_block_returns_the_free_block(self):
        
        relocation_register = Mock()
        first_fit = FirstFit(relocation_register)
        program = [1,2,3,4,5]
        block1 = Mock()
        block2 = Mock()
        array = [block1, block2]
        
        relocation_register.get_free_blocks.return_value = array
        block1.get_size.return_value = 4
        block2.get_size.return_value = 6
        
        expected = block2
        
        actual = first_fit.search_free_block(program)
        
        self.assertEquals(expected, actual)


    def test_alloc(self):
        relocation_register = Mock()
        first_fit = FirstFit(relocation_register)
        program = [1,2,3,4,5]
        block1 = Mock()
        block2 = Mock()
        array = [block1, block2]
        pcb = Mock()
        
        relocation_register.get_free_size.return_value = 10
        relocation_register.get_free_blocks.return_value = array
        block1.get_size.return_value = 4
        block2.get_size.return_value = 6
        pcb.id.return_value = 1
        
        first_fit.alloc(program, pcb)
        
        relocation_register.assign_to_block.assert_called_once_with(program, pcb, block2)
    