import unittest

from main.system.process.pcb import Pcb

class TestLinkedList(unittest.TestCase):

    def setUp(self):
        self.my_pcb = Pcb(1, "ready", 1, 0)

    def test_pcb_state(self):
        self.assertEquals(self.my_pcb.state, "ready")
       
    def test_pcb_id(self):
        self.assertEquals(self.my_pcb.pid, 1)
          
if __name__ == '__main__':
    unittest.main()