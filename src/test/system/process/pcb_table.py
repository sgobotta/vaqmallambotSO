import unittest
from main.system.process.pcb_table import PcbTable
from main.system.process.pcb import Pcb

class TestPcbTable(unittest.TestCase):
    
    def setUp(self):
        
        self.pcb_table  = PcbTable()
        
        self.pcb01      = Pcb(None, None, 1, 1)
            
        self.pcb_table.add(self.pcb01)
    
    def test_add_pcb(self):
        
        expected_id = self.pcb_table.last_pid - 1
        
        self.assertEquals(self.pcb_table.pcb_dict[expected_id], self.pcb01)
        self.assertEquals(self.pcb01.id, expected_id)
        