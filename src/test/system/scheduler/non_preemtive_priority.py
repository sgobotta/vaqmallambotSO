'''
Created on 12 de abr. de 2016

@author: sgobotta
'''
import unittest
from main.system.scheduler.non_preemtive_priority import Priority
from main.system.process.pcb import Pcb
from main.strings.state import STATE

class TestPriorityQueue(unittest.TestCase):

    def setUp(self):
        
        self.pcb1_p1 = Pcb(STATE.READY, "pid01", 1, 0)
        self.pcb2_p1 = Pcb(STATE.READY, "pid02", 1, 0)
        self.pcb1_p2 = Pcb(STATE.READY, "pid03", 2, 0)
        self.pcb2_p2 = Pcb(STATE.READY, "pid04", 2, 0)
        self.pcb1_p3 = Pcb(STATE.READY, "pid05", 3, 0)
        self.pcb2_p3 = Pcb(STATE.READY, "pid06", 3, 0)
        
        self.my_priority_queue = Priority(4)
    
    def test_count_queues(self):
        
        self.assertEquals(self.my_priority_queue.count_queues(), 4)
        
    
    def test_add_pcb_priority_1_to_priority_queue(self):
        
        #Exersice
        self.my_priority_queue.add(self.pcb1_p1)
        
        pcb_priority = self.pcb1_p1.priority
        desired_queue = self.my_priority_queue.get_queue_by_priority(pcb_priority)
        
        self.assertEquals(desired_queue.size(), 1)
        
        
    def test_add_two_pcb_priority_1_to_priority_queue(self):
        
        pcb1_priority = self.pcb1_p1.priority
        pcb2_priority = self.pcb2_p1.priority
        pcb3_priority = self.pcb1_p2.priority
        pcb4_priority = self.pcb2_p2.priority
        
        desired_queue_1 = self.my_priority_queue.get_queue_by_priority(pcb1_priority)
        desired_queue_2 = self.my_priority_queue.get_queue_by_priority(pcb2_priority)
        desired_queue_3 = self.my_priority_queue.get_queue_by_priority(pcb3_priority)
        desired_queue_4 = self.my_priority_queue.get_queue_by_priority(pcb4_priority)
        
        self.my_priority_queue.add(self.pcb1_p1)
        self.assertTrue(desired_queue_1.size(), 1)
        
        self.my_priority_queue.add(self.pcb2_p1)
        self.assertEquals(desired_queue_2.size(), 2)
        
        self.my_priority_queue.add(self.pcb1_p2)
        self.assertEquals(desired_queue_3.size(), 1)
        
        self.my_priority_queue.add(self.pcb2_p2)
        self.assertEquals(desired_queue_4.size(), 2)
        
    def test_get_next_pcb_from_first_priority_queue(self):
        
        self.my_priority_queue.add(self.pcb1_p1)
        self.my_priority_queue.add(self.pcb2_p2)
        next_pcb = self.my_priority_queue.get_next()
        
        self.assertEquals(next_pcb, self.pcb1_p1)
        
    def test_get_next_pcb_from_second_priority_queue(self):
        
        #Adds to the second priority Queue
        self.my_priority_queue.add(self.pcb1_p2)
        self.my_priority_queue.add(self.pcb2_p2)
        self.my_priority_queue.add(self.pcb1_p3)
        
        #Desired Pcb to assert
        next_pcb = self.my_priority_queue.get_next()
                          
        self.assertEquals(next_pcb, self.pcb1_p2)
    
    def test_get_next_pcb_from_third_priority_queue(self):
        
        #Adds to the third priority Queue
        self.my_priority_queue.add(self.pcb1_p3)
        self.my_priority_queue.add(self.pcb2_p3)
        
        #Desired Pcb to assert
        next_pcb = self.my_priority_queue.get_next()
                          
        self.assertEquals(next_pcb, self.pcb1_p3)    
    
    def test_add_many_pcbs_and_get_next_pcb_from_empty_queues(self):
        
        #Adds a few elements for different queues
        self.my_priority_queue.add(self.pcb2_p1)
        self.my_priority_queue.add(self.pcb1_p3)
        self.my_priority_queue.add(self.pcb2_p2)
        self.my_priority_queue.add(self.pcb1_p1)
        self.my_priority_queue.add(self.pcb1_p2)
        self.my_priority_queue.add(self.pcb2_p3)
        
        #Take elements by priority and assert the desired Pcb
        next_pcb = self.my_priority_queue.get_next()
        self.assertEquals(next_pcb, self.pcb2_p1)
        
        next_pcb = self.my_priority_queue.get_next()
        self.assertEquals(next_pcb, self.pcb1_p1)
        
        next_pcb = self.my_priority_queue.get_next()
        self.assertEquals(next_pcb, self.pcb2_p2)

        next_pcb = self.my_priority_queue.get_next()
        self.assertEquals(next_pcb, self.pcb1_p2)

        next_pcb = self.my_priority_queue.get_next()
        self.assertEquals(next_pcb, self.pcb1_p3)
        
        next_pcb = self.my_priority_queue.get_next()
        self.assertEquals(next_pcb, self.pcb2_p3)   
        
    def test_get_next_pcb_from_empty_queues(self):
        
        #Adds a few elements for different queues
        self.my_priority_queue.add(self.pcb1_p3)
        self.my_priority_queue.add(self.pcb1_p2)
        self.my_priority_queue.add(self.pcb2_p2)
        self.my_priority_queue.add(self.pcb1_p1)
        self.my_priority_queue.add(self.pcb2_p3)
        self.my_priority_queue.add(self.pcb2_p1)
        
        #Take all elements from queues
        self.my_priority_queue.get_next()
        self.my_priority_queue.get_next()
        self.my_priority_queue.get_next()
        self.my_priority_queue.get_next()
        self.my_priority_queue.get_next()
        self.my_priority_queue.get_next()
        
        self.assertEqual(self.my_priority_queue.get_next(), None)
    
    if __name__ == '__main__':
        unittest.main()