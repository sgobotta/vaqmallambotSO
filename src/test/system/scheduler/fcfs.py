import unittest
from main.system.scheduler.fcfs import Fcfs

class TestFsFc(unittest.TestCase):

    def setUp(self):
        self.my_fcfs = Fcfs()
        self.my_fcfs.add_ready(1)
    
    def test_01_add(self):
        
        self.assertEquals(self.my_fcfs.get_queue().size(), 1)
        
    def test_get_next(self):
        self.assertEquals(self.my_fcfs.get_next(), 1)
        self.assertEquals(self.my_fcfs.get_queue().size(), 0)
    
    
    
if __name__ == '__main__':

        unittest.main()