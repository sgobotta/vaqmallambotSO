'''
Created on Jun 28, 2016

@author: santiago
'''
import unittest

from mock import Mock
from main.hardware.memory.paging.algorithms.LRU import LRU
from datetime import datetime
from main.system.log.output import Output


class Test(unittest.TestCase):


    def setUp(self):
        
        self.policy = LRU()
        
        self.policy.output = Output()

#     def test_a_table_manager_calls_get_load_indexes_on_get_free_frame(self):
#         
#         is_loaded_indexes = [0, 1, 3, 4, 6, 7]
#         
#         timestamp01 = datetime(2016, 01, 01, 00, 00, 00, 010000)
#         timestamp02 = datetime(2016, 01, 01, 00, 00, 00, 010001)
#         timestamp03 = datetime(2016, 01, 01, 00, 00, 00, 030000)
#         timestamp04 = datetime(2016, 01, 01, 00, 00, 00, 040000)
#         timestamp05 = datetime(2016, 01, 01, 00, 00, 00, 050000)
#         timestamp06 = datetime(2016, 01, 01, 00, 00, 00, 060000)
#         timestamp07 = datetime(2016, 01, 01, 00, 00, 00, 000010)
#         timestamp08 = datetime(2016, 01, 01, 00, 00, 00, 070000)        
#         
#         usage_list = [timestamp01, timestamp02, timestamp03, timestamp04, timestamp05, timestamp06, timestamp07, timestamp08]        
#         
#         table_manager = Mock()
#         table_manager.get_loaded_indexes.return_value = is_loaded_indexes
#         table_manager.get_usage_list.return_value     = usage_list
#         self.policy.set_table_manager(table_manager)
#         
#         self.policy.get_free_frame()
#         
#         table_manager.get_loaded_indexes.assert_called_once_with()
#         
#         expected_index = self.policy.find_oldest_usage_index(is_loaded_indexes)
#         
#         self.assertEquals(expected_index, 6)

    def a_policy_returns_the_index_with_the_oldest_reference_on_find_oldest_usage_index(self):

        timestamp01 = datetime(2016, 01, 01, 00, 00, 00, 300000)
        timestamp02 = datetime(2016, 01, 01, 00, 00, 00, 200000)
        timestamp03 = datetime(2016, 01, 01, 00, 00, 00, 400000)
        timestamp04 = datetime(2016, 01, 01, 00, 00, 00, 700000)
        timestamp05 = datetime(2016, 01, 01, 00, 00, 00, 800000)
        timestamp06 = datetime(2016, 01, 01, 00, 00, 00, 900000)
        
        usage_list = [timestamp01, timestamp02, timestamp03, timestamp04, timestamp05, timestamp06]
        
        table_manager = Mock()
        table_manager.get_usage_list.return_value = usage_list
        
        self.policy.set_table_manager(table_manager)
        
        index_list = [0, 1, 4, 5]
        expected_index = self.policy.find_oldest_usage_index(index_list)
        
        self.assertEquals(expected_index, 1)
        
    def test_table_manager_calls_set_usage_reference_on_LRU_set_usage_reference(self):
        
        time = 190900
        frame = 5
        
        table_manager = Mock()
        self.policy.set_table_manager(table_manager)
        
        self.policy.timestamp_provider = Mock()
        self.policy.timestamp_provider.now.return_value = time
        
        self.policy.on_get_free_frame_action(Mock(), Mock(), frame)
        
        table_manager.set_usage_reference.assert_called_with(5, 190900)
    