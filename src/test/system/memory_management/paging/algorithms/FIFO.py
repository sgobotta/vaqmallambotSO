'''
Created on Jun 30, 2016

@author: santiago
'''
import unittest
from main.hardware.memory.paging.algorithms.FIFO import Fifo
from mock import Mock


class Test(unittest.TestCase):


    def setUp(self):
        
        self.policy = Fifo()
        
        self.policy.set_output(Mock())
        
        self.policy.set_table_manager(Mock())
        
        self.policy.set_swap_manager(Mock())

    def test_a_queue_has_a_size_of_4_when_fifo_add_new_frames(self):
        
        frame_0 = 0
        frame_1 = 1
        frame_2 = 2
        frame_3 = 3
        
        self.policy.add_new_frame(frame_0)
        self.policy.add_new_frame(frame_1)
        self.policy.add_new_frame(frame_2)
        self.policy.add_new_frame(frame_3)

        self.assertEquals(self.policy.frames_queue.size(), 4)
        
    def test_a_policy_returns_its_first_element_in_queue_on_fifo_get_free_frame(self):
        
        frame_0 = 0
        frame_1 = 1
        frame_2 = 2
        frame_3 = 3
        
        self.policy.add_new_frame(frame_0)
        self.policy.add_new_frame(frame_1)
        self.policy.add_new_frame(frame_2)
        self.policy.add_new_frame(frame_3)
        
        free_frame = self.policy.get_free_frame()
        
        self.assertEquals(free_frame, frame_0)
        
    def a_table_manager_gets_frame_number_by_pid_page_on_get_free_frame_action(self):
        
        pid            = 1
        page           = 0
        expected_frame = 9
        
        self.policy.table_manager = Mock()
        self.policy.table_manager.get_frame_number_by.return_value = expected_frame
        
        self.policy.on_get_free_frame_action(pid, page)
        
        free_frame = self.policy.get_free_frame()
        
        self.policy.table_manager.get_frame_number_by.assert_called_once_with(pid, page)
        self.assertEquals(free_frame, expected_frame)
        