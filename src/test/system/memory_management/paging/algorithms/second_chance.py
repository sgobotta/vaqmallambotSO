'''
Created on Jun 30, 2016

@author: santiago
'''
import unittest
from main.hardware.memory.paging.algorithms.second_chance import SecondChance
from main.hardware.memory.paging.paging_manager import PagingTableManager
from mock import Mock


class Test(unittest.TestCase):

    def setUp(self):
        
        self.policy = SecondChance()
        
        self.policy.set_table_manager(PagingTableManager())

    def test_a_queue_has_a_size_of_4_when_fifo_add_new_frames(self):
        
        frame_0 = 0
        frame_1 = 1
        frame_2 = 2
        frame_3 = 3
        
        self.policy.add_new_frame(frame_0)
        self.policy.add_new_frame(frame_1)
        self.policy.add_new_frame(frame_2)
        self.policy.add_new_frame(frame_3)

        self.assertEquals(self.policy.frames_queue.size(), 4)
        
#     def test_a_policty_returns_the_frame_with_number_3_on_get_free_frame(self):
#         
#         self.policy.paging_manager = PagingTableManager()
#         
#         frame_list = [ 9, 1, 6, 3, 5]
#         usage_list = [ 1, 1, 1, 0, 0]
#         
#         self.policy.frames_queue.queue(9)
#         self.policy.frames_queue.queue(1)
#         self.policy.frames_queue.queue(6)
#         self.policy.frames_queue.queue(3)
#         self.policy.frames_queue.queue(5)
#         
#         self.policy.paging_manager.paging_table = Mock()
#         self.policy.paging_manager.paging_table.get_frame_list.return_value = frame_list
#         self.policy.paging_manager.paging_table.get_usage_list.return_value = usage_list
#         
#         free_frame = self.policy.get_free_frame()
#         
#         self.assertEquals(free_frame, 3)