'''
Created on Jun 12, 2016

@author: santiago
'''
import unittest
from main.hardware.memory.paging.paging_table import PagingTable
from mock import Mock


class Test(unittest.TestCase):

    def setUp(self):
        
        self.paging_table = PagingTable()

    def test_frames_list_has_an_element_on_paging_table_set_frame_at(self):
        
        index    = 0
        frame_id = 5
        
        #Se cumple precondicion para set_frame_at
        self.paging_table.add_reference(50, 100, Mock())
        
        self.paging_table.set_frame_at(index, frame_id)
        
        self.assertEqual(self.paging_table.get_frame_list().index(frame_id), 0)


    def test_pid_list_and_page_list_have_a_new_element_on_add_reference(self):
        
        pid  = 6
        page = 7
        
        self.paging_table.add_reference(pid, page, Mock())
        
        self.assertTrue(6 in self.paging_table.get_pid_list())
        self.assertTrue(7 in self.paging_table.get_page_list())
        
    def test_the_value_in_a_slot_in_the_usage_list_changes_on_set_usage_reference(self):
    
        pid  = 5
        page = 10
    
        frame = 8
        
        self.paging_table.add_reference(pid, page, Mock())
        
        self.paging_table.set_frame_at(0, frame)
        print self.paging_table.table
        value = 7
        
        self.paging_table.set_usage_reference_at(frame, value)
        
        
        self.assertEquals(self.paging_table.get_usage_list()[0], 7)