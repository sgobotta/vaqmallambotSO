'''
Created on Jun 18, 2016

@author: santiago
'''
import unittest
from main.hardware.memory.paging.swap_manager import SwapManager
from mock import Mock


class Test(unittest.TestCase):


    def setUp(self):
        
        self.memory_size = 65536
        
        self.manager = SwapManager(self.memory_size, Mock(), Mock())
        
        self.manager.set_loader(Mock())
        
        self.manager.set_output(Mock())
        
    def test_a_swap_manager_has_a_swap_memory_twice_the_size_of_the_memory_size_given(self):
        
        self.assertEquals(len(self.manager.swap_memory), self.memory_size * 2)
