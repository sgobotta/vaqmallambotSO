'''
Created on Jun 11, 2016

@author: santiago
'''
import unittest
from main.hardware.memory.paging.paging_strategy import Paging
from mock import Mock

class Test(unittest.TestCase):
    
    def setUp(self):

        frame_size = 256
        mem_size   = 65536
        
        self.strategy = Paging(frame_size, mem_size)
        
        self.strategy.paging_table.table[0] = [ 1, 3, 1, 7, 2, 3,40] # Pid list
        self.strategy.paging_table.table[1] = [ 0, 0, 1, 0, 1, 1, 0] # Page list
        self.strategy.paging_table.table[2] = [ 1, 2, 3, 4,-1, 5,-1] # Frame list
        self.strategy.paging_table.table[3] = [ 0, 1, 1, 1, 0, 1, 0] # Load list
        self.strategy.paging_table.table[4] = [-1,-1,-1,-1,-1,-1, 5] # Swap list

        loader = Mock()
        self.strategy.set_loader(loader)
        
        self.strategy.policy = Mock()
        
        output = Mock()
        
        self.strategy.set_output(output)
        
        self.strategy.swap_manager.set_loader(loader)
        self.strategy.swap_manager.set_output(output)
        
        self.strategy.interruption_manager = Mock()
        
    def test_a_strategy_returns_a_base_address_of_1024_when_a_given_pid_and_page_are_associated_in_the_fourth_frame(self):
        
        pid_id   = 7
        pid_page = 0
        
        base_address = self.strategy.get_base_address(pid_id, pid_page)
        
        self.assertEquals(base_address, 1024)
            
    def test_a_strategy_returns_a_base_address_of_256_when_a_given_pid_and_page_are_swapped_out(self):
        
        # Ocupa el primer frame de la tabla, considerando usadas las address desde 0 a 255 
        self.strategy.frames_table.add_reference_at(0)
        
        pid_id   = 40
        pid_page = 0
        
        base_address = self.strategy.get_base_address(pid_id, pid_page)
        
        current_frame  = self.strategy.get_frame_number_by(pid_id, pid_page)
        expected_frame = 1
        
        self.assertEquals(base_address, 256)
        self.assertEquals(current_frame, expected_frame)
        
#     def test_a_strategy_returns_a_base_address_of_768_when_a_given_pid_and_page_were_not_loaded_in_the_table(self):
# 
#         # Ocupa los primeros tres frames de la tabla, considerando usadas las address desde 0 a 765 
#         self.strategy.frames_table.add_reference_at(0)
#         self.strategy.frames_table.add_reference_at(1)
#         self.strategy.frames_table.add_reference_at(2)
#         
#         pid_id   = 79
#         pid_page = 50
#         
#         #Se cumple precondicion para get_base_address
#         self.strategy.paging_table_manager.add_to_table(pid_id, 50, Mock())
#         
#         base_address   = self.strategy.get_base_address(pid_id, pid_page)
# 
#         current_frame  = self.strategy.paging_table_manager.get_frame_number_by(pid_id, pid_page)
#         expected_frame = 3
#         
#         #self.assertEquals(base_address, 768)
# 
#         self.assertEquals(current_frame, expected_frame)