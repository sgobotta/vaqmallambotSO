'''
Created on Jun 9, 2016

@author: santiago
'''
import unittest
from mock import Mock
from main.hardware.memory.paging.paging_manager import PagingTableManager
from main.hardware.memory.paging.paging_table import PagingTable


class Test(unittest.TestCase):

    def setUp(self):
        
        paging_table = PagingTable()
        paging_table.table[0] = [ 1, 3, 1, 7, 2] # Pid list
        paging_table.table[1] = [ 0, 0, 1, 0, 1] # Page list
        paging_table.table[2] = [ 1, 2, 3, 4,-1] # Frame list
        paging_table.table[3] = [ 0, 1, 1, 1, 0] # Load list
        paging_table.table[4] = [-1, 1, 1, 1, 8] # Swap list
        paging_table.table[5] = [-1,-1,-1,-1,-1] # Usage list
        
        self.manager = PagingTableManager(paging_table)
        
        self.assertFalse(self.manager.is_pid_referenced(5))
        
    def test_a_pid_is_in_a_paging_table(self):
        
        pid_id = 1
        
        self.assertTrue(self.manager.is_pid_referenced(pid_id))

    def test_a_page_number_is_referenced_in_a_paging_table(self):

        pid_id   = 1
        pid_page = 1
        
        self.assertTrue(self.manager.is_page_referenced(pid_id, pid_page))
        
    def test_a_page_is_loaded_in_a_paging_table(self):
        
        pid_id   = 1
        pid_page = 1
        
        self.assertTrue(self.manager.is_page_loaded(pid_id, pid_page))

    def test_a_manager_gets_the_index_of_a_pid_page(self):
        pid_id   = 3
        pid_page = 0
        
        self.assertEquals(self.manager.get_page_index(pid_id, pid_page), 1)
        
    def test_a_manager_gets_the_frame_number_of_a_pid_page(self):
        pid_id   = 7
        pid_page = 0
        
        self.assertEquals(self.manager.get_frame_number_by(pid_id, pid_page), 4)        

    def test_a_manager_calls_set_frame_at_on_load_page_with_frame(self):
        
        self.manager.paging_table = Mock()
        self.manager.paging_table.get_pid_list.return_value   = [1, 3, 1, 7, 2]
        self.manager.paging_table.get_page_list.return_value  = [0, 0, 1, 0, 1]
        self.manager.paging_table.get_load_list.return_value  = [0, 0, 0, 0, 0]
        self.manager.paging_table.get_frame_list.return_value = [0, 0, 0, 0, 0]  
        
        pid_id              = 3
        pid_page            = 0
        frame_number        = 9
        expected_page_index = 1

        self.manager.load_page_with_frame(pid_id, pid_page, frame_number)
        
        self.manager.paging_table.set_frame_at.assert_called_once_with(expected_page_index, frame_number)
        self.manager.paging_table.set_load_at.assert_called_with(expected_page_index)
        
    def test_a_new_reference_is_added_in_the_table(self):

        pid_id   = 10
        pid_page = 0
        
        self.manager.add_to_table(pid_id, pid_page, Mock())
        
        self.assertTrue(self.manager.is_page_referenced(10, 0))
        
    def test_an_existing_reference_in_a_table_is_not_added(self):

        self.manager.paging_table = Mock()
        self.manager.paging_table.get_pid_list.return_value  = [1, 3, 1, 7, 2]
        self.manager.paging_table.get_page_list.return_value = [0, 0, 1, 0, 1]
        
        pid_id   = 3
        pid_page = 0
        
        self.manager.add_to_table(pid_id, pid_page, Mock())
        
        self.manager.paging_table.add_reference.assert_has_calls([])
        
    def test_a_page_in_the_table_is_loaded(self):
        
        pid_id   = 3
        pid_page = 0
                
        self.assertTrue(self.manager.is_page_loaded(pid_id, pid_page))
        
    def test_a_page_in_the_table_is_not_loaded(self):

        pid_id   = 2
        pid_page = 1
                
        self.assertFalse(self.manager.is_page_loaded(pid_id, pid_page))
        
    def test_a_paging_table_is_updated_on_load_page_from_swap_at_frame(self):
        
        self.manager.paging_table = Mock()
        self.manager.paging_table.get_pid_list.return_value   = ["dummyElement", "dummyElement", 2]
        self.manager.paging_table.get_page_list.return_value  = ["dummyElement", "dummyElement", 1]
        self.manager.paging_table.get_frame_list.return_value = ["dummyElement", "dummyElement",-1]
        self.manager.paging_table.get_load_list.return_value  = ["dummyElement", "dummyElement", 0]
        
        pid_id     = 2
        pid_page   = 1
        free_frame = 5
        
        index = 2
        
        self.manager.load_page_from_swap_at_frame(pid_id, pid_page, free_frame)
        
        self.manager.paging_table.set_frame_at.assert_called_once_with(index, 5)
        self.manager.paging_table.set_load_at(index)
        self.manager.paging_table.remove_swap_reference_at.assert_called_once_with(index)
        
        
    def test_a_list_of_elements_that_are_loaded_contains_three_elements_with_value_1_2_and_3(self):
        
        loaded_list_indexes = self.manager.get_loaded_indexes()
        
        self.assertEquals(len(loaded_list_indexes), 3)
        self.assertEquals(loaded_list_indexes[0], 1)
        self.assertEquals(loaded_list_indexes[1], 2)
        self.assertEquals(loaded_list_indexes[2], 3)
        
    def test_paging_table_set_usage_reference_at_is_called_on_paging_manager_set_usage_reference(self):
        
        frame = 5
        value = 'Im a usage reference'
        
        self.manager.paging_table = Mock()
        self.manager.paging_table.get_pid_list.return_value   = [ 1, 3, 1, 7, 2]
        self.manager.paging_table.get_page_list.return_value  = [ 0, 0, 1, 0, 1]
        self.manager.paging_table.get_frame_list.return_value = [ 5,-1,-1,-1,-1]
        
        self.manager.set_usage_reference(frame, value)
        
        self.manager.paging_table.set_usage_reference_at.assert_called_once_with(5, 'Im a usage reference')
        