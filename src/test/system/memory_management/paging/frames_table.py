'''
Created on Jun 18, 2016

@author: santiago
'''
import unittest
from main.hardware.memory.paging.frames_manager import FramesManager
from main.hardware.memory.paging.exceptions import FullFramesTableException


class Test(unittest.TestCase):

    
    def setUp(self):
        
        self.max_size   = 65536
        self.frame_size = 256
        
        self.table = FramesManager(self.max_size, self.frame_size)
    
    def test_a_table_with_no_referenced_frames_has_a_number_of_free_slots_equals_to_its_max_size(self):
        
        self.assertEquals(self.table.free_slots(), self.max_size)
    
    def test_a_table_with_no_referenced_frames_has_zero_used_slots(self):
        
        self.assertEquals(self.table.used_slots(), 0)
            
    def test_an_empty_table_adds_a_new_frame_and_its_used_and_free_slots_vary_by_one(self):
        
        self.table.add_reference_at(1)
        
        self.assertEquals(self.table.free_slots(), self.max_size - 1)
        self.assertEquals(self.table.used_slots(), 1)
        
        
    def test_a_fully_referenced_table_cant_add_new_references(self):
        
        for i in range(self.table.max_size): self.table.table[i] = 1
        
        self.assertRaises(FullFramesTableException, self.table.add_reference_at, 100)
        
    def test_a_table_gets_a_base_address_of_32768_on_a_frame_at_position_128(self):
        
        expected_base_address = self.table.get_base_address(128)
        
        self.assertEquals(expected_base_address, 32768)
        
    def test_a_table_gets_the_first_index_of_an_element_that_is_equal_to_zero_on_get_free_frame(self):
        
        self.table.add_reference_at(0)
        self.table.add_reference_at(1)
        self.table.add_reference_at(2)
        
        free_frame = self.table.get_free_frame()
        
        self.assertEquals(free_frame, 3)
        
        