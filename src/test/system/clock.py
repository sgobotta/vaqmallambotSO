from unittest import TestCase
from mock import Mock
from main.system.clock import Clock
from main.system.log.output import Output

class ClockTest(TestCase):
    
    def test_listeners_process_on_tick(self):
        
        listener = Mock()
        clock = Clock(Output())
        clock.add_listener(listener)
        param = object()
        clock.ticks()
        listener.process.assert_called_with()
        
    def test_first_of_two_listeners_process_on_tick(self):
        listener = Mock()
        listener2 = Mock()
        clock = Clock(Output())
        clock.add_listener(listener)
        clock.add_listener(listener2)
        param = object()
        clock.ticks()
        listener.process.assert_called_with()
    