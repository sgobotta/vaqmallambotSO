from unittest import TestCase
from main.system.process.pcb import Pcb
from main.system.dispatcher import Dispatcher
from mock import Mock

class TestDispatcher(TestCase):
    
    def setUp(self):
        
        self.pcb1       = Pcb(1, "ready", 1, 1)
        self.pcb2       = Pcb(2, "ready", 1, 6)
        
    def test_dispatcher_updates_pcb_on_update_pcb(self):
        
        cpu = Mock()
        
        dispatcher = Dispatcher(cpu)
        
        dispatcher.cpu.pc  = 10
        dispatcher.pcb = self.pcb2
        
        dispatcher.update_pcb()
        
        self.assertEquals(dispatcher.pcb.pc, 10)
        
    def test_dispatcher_updates_cpu_registers_on_load(self):
        
        cpu = Mock()
        
        cpu.pc.return_value = self.pcb2.pc
        cpu.base_address.return_value = self.pcb2.base_address
        
        dispatcher = Dispatcher(cpu)
        dispatcher.pcb = self.pcb2
        dispatcher.load()
        
        self.assertEquals(dispatcher.pcb.pc, cpu.pc)
        