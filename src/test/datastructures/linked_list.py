from main.datastructures.linked_list import List 
import unittest

class TestLinkedList(unittest.TestCase):

    def setUp(self):
        self.my_list = List()
        self.my_list.put(1)
        self.my_list.put(2)
        self.my_list.put(3)
        self.my_list.put(4)
        self.my_list.put(5)
        self.my_list.put(6)
        self.my_list.put(7)
        self.my_list.put(8)

    def test_add_element(self):
        self.assertEquals(self.my_list.first.value, 1)
        self.assertEquals(self.my_list.first.next.value, 2)
        self.assertEquals(self.my_list.first.next.next.value, 3)
        self.assertEquals(self.my_list.last.value, 8)
        
        self.assertTrue(self.my_list.contains(1))
        self.assertTrue(self.my_list.contains(2))
        self.assertTrue(self.my_list.contains(3))
        self.assertTrue(self.my_list.contains(4))
      
    def test_remove_element(self):
        self.my_list.remove(1)
        self.my_list.remove(4)
        self.my_list.remove(5)
        self.my_list.remove(6)
        self.my_list.remove(8)
        
        self.assertEquals(self.my_list.first.value, 2)
        self.assertEquals(self.my_list.last.value, 7)
        
        self.assertFalse(self.my_list.contains(1))
        self.assertFalse(self.my_list.contains(4))
        self.assertFalse(self.my_list.contains(5))
        self.assertFalse(self.my_list.contains(6))
        self.assertFalse(self.my_list.contains(8))
        
        self.assertTrue(self.my_list.contains(2))
        self.assertTrue(self.my_list.contains(3))
        self.assertTrue(self.my_list.contains(7))
        
        self.assertEquals(self.my_list.length, 3)
        
        def test_remove_all_elements(self):
        
            self.my_list.remove(1)
            self.my_list.remove(2)
            self.my_list.remove(3)
            self.my_list.remove(4)
            self.my_list.remove(5)
            self.my_list.remove(6)
            self.my_list.remove(7)
            self.my_list.remove(8)
        
            self.assertEuqals(self.my_list.first, None)
            self.assertEquals(self.my_list.last, None)
          
    def test_get_element(self):
      
        self.assertEquals(self.my_list.get(1), 1)
        self.assertEquals(self.my_list.get(2), 2)
        self.assertEquals(self.my_list.get(3), 3)
        self.assertEquals(self.my_list.get(4), 4)
    
    def test_length(self):
      
        self.assertEquals(self.my_list.length, 8)
    
if __name__ == '__main__':

        unittest.main()