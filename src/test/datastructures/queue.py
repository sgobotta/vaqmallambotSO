import unittest
from main.datastructures.queue import Queue

class TestQueue(unittest.TestCase):
    
    def setUp(self):
        self.my_queue = Queue()
        self.my_queue.queue(1)
        
    def test_dequeue(self):
        self.assertEquals(self.my_queue.dequeue(), 1)
        self.assertEquals(self.my_queue.size(), 0)
        self.assertTrue(self.my_queue.is_empty)
        
    def test_queue(self):
        self.my_queue.queue(2)
        self.assertEquals(self.my_queue.dequeue(), 1)
        self.assertEquals(self.my_queue.size(), 1)

    def test_queue_adding_more_elements(self):
        
        self.my_queue.queue(2)
        self.my_queue.queue(3)
        self.my_queue.queue(4)
        self.my_queue.queue(5)
        
        self.assertEquals(self.my_queue.size(), 5)
        self.assertEquals(self.my_queue.dequeue(), 1)
        self.assertEquals(self.my_queue.dequeue(), 2)
        self.assertEquals(self.my_queue.dequeue(), 3)
        self.assertEquals(self.my_queue.dequeue(), 4)
        self.assertEquals(self.my_queue.dequeue(), 5)
        
        self.assertEquals(self.my_queue.size(), 0)
        