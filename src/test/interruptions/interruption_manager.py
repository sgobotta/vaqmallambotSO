from unittest import TestCase
from main.interruptions.interruption_manager import InterruptionManager
from mock import Mock
from main.strings.instruction import INSTRUCTION

class TestInterruptionManager(TestCase):
    
    def setUp(self):
        
        self.end_instruction            = INSTRUCTION.END
        self.io_instruction             = INSTRUCTION.IO
        self.end_io_instruction         = INSTRUCTION.OUT_IO
        self.time_out_instruction       = INSTRUCTION.TIME_OUT
        self.new_instruction            = INSTRUCTION.NEW
        self.handler                    = Mock()
        self.int_manager                = InterruptionManager()
        self.int_manager.set_handler(self.handler)
    
    def test_interruption_manager_calls_evaluate_irq_on_handle_when_irq_is_an_end_instruction(self):
        
        self.int_manager.handle(self.end_instruction)
        
        
        self.handler.handle.assert_called_with(self.end_instruction, self.int_manager)
        
    
    def test_interruption_manager_calls_evaluate_irq_on_handle_when_irq_is_an_io_instruction(self):
        
        self.int_manager.handle(self.io_instruction)
        
        self.handler.handle.assert_called_with(self.io_instruction, self.int_manager)
        
    def test_interruption_manager_calls_evaluate_irq_on_handle_when_irq_is_not_an_end_io_instruction(self):
        
        self.int_manager.handle(self.end_io_instruction)
        
        self.assertEquals(self.int_manager.irqCPU, None)
        self.handler.handle.assert_called_with(self.end_io_instruction, self.int_manager)
        
    
    def test_interruption_manager_calls_evaluate_irq_on_handle_when_irq_is_a_time_out_instruction(self):
        
        self.int_manager.handle(self.time_out_instruction)
        
        self.handler.handle.assert_called_with(self.time_out_instruction, self.int_manager)
        
    def test_interruption_manager_calls_evaluate_irq_on_handle_when_irq_is_a_new_interruption(self):
        
        self.int_manager.handle(self.new_instruction)
        
        self.handler.handle.assert_called_with(self.new_instruction, self.int_manager)
    
        