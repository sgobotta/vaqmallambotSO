from unittest import TestCase
from main.interruptions.new import New
from mock import Mock
from main.strings.instruction import INSTRUCTION

class TestNewIRq(TestCase):
    
    def test_new_interruption_reads_program_in_disc_on_execute(self):
        
        interruption_manager = Mock()
        
        interruption = New('sublime', interruption_manager)
        
        interruption.execute()
        
        interruption_manager.read.assert_called_once_with('sublime')
        
        interruption.output.print_log()
        
#     def test_new_interruption_loads_program_in_memory_on_execute(self):
#         
#         file_system = { 'sublime' : [INSTRUCTION.NEW + 'sublime',
#                                      'open',
#                                      INSTRUCTION.IO,
#                                      INSTRUCTION.END] }
#         
#         disc                    = Mock()
#         disc.file_system        = file_system
#         interruption_manager    = Mock()
#         interruption            = New('sublime', interruption_manager)
#         program                 = file_system['sublime']
#         
#         interruption_manager.read.return_value = program
#         
#         interruption.execute()
#         
#         interruption_manager.load_in_memory.assert_called_once_with(program)
#         