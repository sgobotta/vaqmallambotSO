'''
Created on 22/6/2016

@author: leonardo
'''
from main.interruptions.handler import Handler
from mock import Mock
from main.strings.instruction import INSTRUCTION
from unittest import TestCase

class Test_handler(TestCase):


    def setUp(self):
        self.end_instruction            = INSTRUCTION.END
        self.io_instruction             = INSTRUCTION.IO
        self.end_io_instruction         = INSTRUCTION.OUT_IO
        self.time_out_instruction       = INSTRUCTION.TIME_OUT
        self.new_instruction            = INSTRUCTION.NEW
        self.interruption_manager       = Mock()
        self.handler                    = Handler()


    def test_handler_handle_a_end_instruction(self):
        pass