from unittest import TestCase
from main.strings.state import STATE
from mock import Mock
from main.system.process.pcb import Pcb
from main.interruptions.io_in import IOIn
from main.strings.instruction import INSTRUCTION
from main.system.log.output import Output

class TestIOInInterruptionRequest(TestCase):
    
    def test_time_out_interruption_puts_a_pcb_on_a_device_queue_on_execute(self):
              
        pcb                     = Pcb(01, STATE.RUNNING, 3, 0)
        device                  = Mock()
        interruption_manager    = Mock()
          
        io_interruption         = IOIn(INSTRUCTION.IO + 'displays something', interruption_manager)
        
        interruption_manager.get_pcb_from_dispatcher.return_value = pcb
        #interruption_manager.get_next_from_scheduler.return_value = pcb2
        interruption_manager.is_dispatcher_empty.return_value = False 
        interruption_manager.get_device_by_name.return_value = device
        #interruption_manager.add_to_device_queue.return_value = None 
        
        interruption_manager.output = Output()
        
        io_interruption.execute()
        
        interruption_manager.add_to_device_queue.assert_called_once_with(pcb, device)

        #interruption_manager.output.print_log()