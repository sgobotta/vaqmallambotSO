from unittest import TestCase
from mock import Mock
from main.interruptions.io_end import IOEnd
from main.strings.instruction import INSTRUCTION
from main.strings.state import STATE


class TestIOEndIRq(TestCase):
    
    
#     def test_io_end_interruption_calls_scheduler_add_on_execute(self):
#     
#         pcb = Mock()
#     
#         interruption_manager = Mock()
#     
#         interruption_manager.get_next_from_device_queue.return_value = pcb
#         
#         interruption = IOEnd(INSTRUCTION.OUT_IO + 'screen', interruption_manager)
#     
#         interruption.execute()
#         
#         interruption_manager.add_to_scheduler.assert_called_with(pcb)
#         self.assertEquals(pcb.state, STATE.READY)
#         
        #interruption.output.print_log()
        
    def test_io_end_interruption_calls_dispatcher_get_next_on_execute_when_cpu_is_idle(self):
        
        interruption_manager = Mock()
        interruption_manager.is_running_process.return_value = None
        
        interruption = IOEnd(INSTRUCTION.OUT_IO + 'keyboard', interruption_manager)
        
        interruption.execute()
        
        interruption_manager.get_next_from_scheduler.assert_called_once_with()
        
        #interruption.output.print_log()