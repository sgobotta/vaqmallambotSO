from unittest import TestCase
from main.system.dispatcher import Dispatcher
from mock import Mock
from main.interruptions.time_out import TimeOut
from main.strings.instruction import INSTRUCTION

class TestTimeOutIRq(TestCase):
    
    def test_time_out_interruption_does_context_switching_on_execute(self):
        
        pcb1                                    = Mock()
        pcb2                                    = Mock()
        
        interruption_manager = Mock()
        
        interruption = TimeOut(INSTRUCTION.TIME_OUT, interruption_manager)

        interruption_manager.get_pcb_from_dispatcher.return_value = pcb1
        interruption_manager.get_next_from_scheduler.return_value = pcb2
        
        interruption.execute()
        
        interruption_manager.dispatcher_update.assert_called_with()
        interruption_manager.save_pcb.assert_called_once_with(pcb2)
        interruption_manager.add_to_scheduler.assert_called_once_with(pcb1)
        
        #interruption.output.print_log()