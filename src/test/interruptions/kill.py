from unittest import TestCase
from main.interruptions.kill import Kill
from main.strings.instruction import INSTRUCTION

from main.interruptions import interruption_manager
from main.system.log.output import Output
from mock import Mock

#class TestKillIRq(TestCase):
    
#     def test_kill_irq_calls_remove_pcb_on_execute(self):
#         
#         pcb                  = Mock()
#         pcb.id.return_value  = 1
#         
#         interruption_manager = Mock()
#         interruption_manager.get_running_pcb.return_value = pcb
#         interruption_manager.output = Output() 
#         
#         interruption = Kill(INSTRUCTION.END + " syndaemon", interruption_manager)
#         
#         interruption.execute()
#         
#         interruption_manager.remove_from_pcb_table.assert_called_once_with(pcb.id)
#         
        
    
#     def test_kill_irq_calls_dispatcher_context_switching_on_excute(self):
#         
#         interruption_manager = Mock()
#         running_pcb = Mock()
#         running_pcb.id.return_value = 1
#         interruption_manager.get_running_pcb.return_value = running_pcb
#         interruption_manager.output = Output()
#         
#         pcb = Mock()
#         interruption_manager.get_next_from_scheduler.return_value = pcb
#         interruption = Kill(INSTRUCTION.END + " sublime", interruption_manager)
#         
#         interruption.execute()
#         
#         interruption_manager.remove_from_pcb_table.assert_called_with(running_pcb.id)
#         interruption_manager.dispatcher_save_pcb()
#         
#         