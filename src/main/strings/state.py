class STATE(object):
    
    NEW     = 'NEW'
    READY   = 'READY'
    RUNNING = 'RUNNING'
    WAITING = 'WAITING'
    KILL    = 'TERMINATED'