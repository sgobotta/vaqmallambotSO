class Node:

    def __init__(self, value):

        self.value    = value
        self.next     = None
        self.previous = None
    
    def __call__(self):
        return self

    def value(self):
        return self.value

    def set_next(self, elem):
        self.next = elem

    def set_previous(self, elem):
        self.previous = elem