from main.datastructures.node import Node

class List:
    def __init__(self):
        self.first   = None
        self.last    = None
        self.length  = 0
        
    def head(self):
        if self.first is not None:
            return self.first.value
    
    def take_tail(self):
        if self.first is not None:
            self.remove(self.first.value)
    
    def is_empty(self):
        return self.first == None
    
    # Adds a new node to self
    def put(self, value):
        elem = Node(value)
        #If the list is empty
        if self.first is None:
            self.first = elem
            self.last  = elem
        else:
            elem.set_previous(self.last)
            self.last.set_next(elem)
            self.last = elem
        self.length += 1
            
    # Removes the first element with value "value"
    def remove(self, value):
        current = self.first
        # No estoy en una lista vacia y no encontre el elemento que buscaba.
        while current is not None and current.value is not value:
            current = current.next
        # Si sali del while y el current no es vacio, entonces encontre al elemento que buscaba
        if current is not None:
            # si no es el primer elemento de la lista
            if current.previous is not None:
                current.previous.set_next(current.next)
            else:
                self.first = current.next
            # si no es el ultimo de la lista
            if current.next is not None:
                current.next.set_previous(current.previous)
            else:
                self.last = current.previous
            
            del current
            self.length -= 1
            
    # Returns the first element with value "value"
    def get(self, value):
        current = self.first
        target = None
        if current is not None:
            while current.value is not value and current is not None:
                current = current.next
            if current.value is value:
                target = current.value
            else:
                #Throw Exception
                pass
        return target

    # Search for an element with value "value" to return a Boolean
    def contains(self, value):
        current = self.first
        while current is not None and current.value is not value:
            current = current.next
        return current is not None
    
    # Returns the number of elements contained
    def length(self):
        return self.length
    
    def first(self):
        return self.first
    
    def last(self):
        return self.last

    
    
    
