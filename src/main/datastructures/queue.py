'''
Created on 1 de abr. de 2016

@author: ignacio y el Gabo
'''
from main.datastructures.linked_list import List

class Queue:

    def __init__(self):
        self.representation = List()

    def queue(self, value):
        self.representation.put(value)

    def dequeue(self):
        first = self.representation.head()
        self.representation.take_tail()
        return first
    
    def is_empty(self):
        return self.representation.is_empty()
        
    def size(self):
        return self.representation.length
    
    def first(self):
        return self.representation.first()