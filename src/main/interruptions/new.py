from main.interruptions.interruption_request import InterruptionRequest
from main.system.log.output import Output
from termcolor import colored
from random import randrange

class New(InterruptionRequest):
    
    def __init__(self, program_name, interruption_manager=None):
        InterruptionRequest.__init__(self, "NewInterruption")
        self.interruption_manager = interruption_manager
        self.program_name = program_name
        self.program = None
        self.irq = "./"
        self.output = Output()
        self.pcb_to_add = None

    def execute(self):
            # Se busca un programa en disco.
        self.read()
        
            # Se busca la proxima celda libre de la memoria.
        free_base_address = self.interruption_manager.get_free_base_address()
        new_pcb           = self.create_pcb()
        new_pcb.set_base_address(free_base_address)
        
            # Se carga el programa en la memoria
        self.load_in_memory(new_pcb)
            # Se busca una prioridad random entre 1 y 3 hasta parametrizar el scheduler
        priority = randrange(1,3)
        new_pcb.priority = priority 
        
        # Valido si hay un proceso corriendo y decido quien entra en cpu
        if self.interruption_manager.is_process_running:
            self.interruption_manager.add_to_scheduler(new_pcb)
            self.pcb_to_add = self.interruption_manager.get_next_from_scheduler()
            self.interruption_manager.dispatcher_save_pcb(self.pcb_to_add)
        else:
            self.interruption_manager.dispatcher_save_pcb(new_pcb)
        self.log_process_creation(new_pcb)
        
    def create_pcb(self):    
        return self.interruption_manager.create_pcb()
        
    def read(self):
        self.program = self.interruption_manager.read(self.program_name)
            
    def load_in_memory(self, pcb):
        self.interruption_manager.load_in_memory(self.program, pcb) 

    # Log methods
    
    def log_process_creation(self, pcb):
        self.interruption_manager.output.log(colored("[ NEW INTERRUPTION ]", "green", attrs=["bold"]) + " >> process created with id: " + str(pcb.pid) + " and priority: " + str(pcb.priority))