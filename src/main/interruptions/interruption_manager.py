
class InterruptionManager:
    
    def __init__(self, kernel=None, dispatcher=None, output=None, handler=None):
        self.irqCPU      = None
        self.pendingList = []
        self.kernel      = kernel
        self.dispatcher  = dispatcher
        self.output      = output
        self.handler     = handler

    def set_kernel(self, kernel):
        self.kernel = kernel

    def set_dispatcher(self, dispatcher):
        self.dispatcher = dispatcher

    def set_output(self, output):
        self.output = output
        
    def set_handler(self,handler):
        self.handler = handler

    def handle(self, irq):
        request = self.handler.handle(irq,self)
        
        if(request.type == "CPUInterruption"):
            self.irqCPU = request
        elif request.type == "PageFault":
            request.execute()
        else:
            self.pendingList.append(request)
        
    def process(self):
        if(self.irqCPU != None):
            self.irqCPU.execute()
            self.irqCPU = None
        for irq in self.pendingList:
            irq.execute()
            self.pendingList = []
        
    def is_interruption_instruction(self, instruction):        
        end_condition       = instruction.lower().startswith('end')
        io_condition        = instruction.lower().startswith('io')
        new_condition       = instruction.lower().startswith('./')
        out_condition       = instruction.lower().startswith('out_')
        page_fault          = instruction.lower().startswith('page_fault')
        return end_condition or io_condition or new_condition or out_condition or page_fault
    
    
    # Comenzando a aislar system de io_end: se crearon los siguientes mensajes
    # para que interactue unicamente con interruption:_manager
    
    def get_next_from_device_queue(self, device):
        return self.kernel.get_next_from_device_queue(device)
        
    def add_to_scheduler(self, pcb):
        self.kernel.add_to_scheduler(pcb)
        
    def is_running_process(self):
        return self.kernel.get_running_process() != None
        
    def get_next_from_scheduler(self):
        return self.kernel.get_next_from_scheduler()
        
    def add_to_device_queue(self, pcb, device):
        self.kernel.add_to_device_queue(pcb, device)
        
    def remove_from_device_queue(self, device):
        self.kernel.remove_from_device_queue(device)
        
    def add_to_pcb_table(self, pcb):
        self.kernel.add_to_pcb_table(pcb)

    def dispatcher_update(self):
        self.dispatcher.update_pcb()
        
    def get_pcb_from_dispatcher(self):
        if self.dispatcher.pcb is not None:
            return self.dispatcher.pcb
        
    def dispatcher_save_pcb(self, pcb):
        if not self.is_process_running and pcb is not None:
            self.kernel.set_running_process(pcb)
            self.dispatcher.save_pcb(pcb)
            
        elif self.is_process_running and pcb is not None:
            self.add_to_scheduler(pcb)       
        else:
            self.clean_cpu_registers()
            self.dispatcher.log_queue_empty_status()
            
    def clean_cpu_registers(self):
        self.dispatcher.clean_cpu_registers()

    def is_dispatcher_empty(self):
        return self.dispatcher.pcb is None
        
    def get_running_pcb(self):
        return self.kernel.running_process

    def remove_from_pcb_table(self, pcb):
        self.kernel.remove_from_pcb_table(pcb)
        
    # New Interruption
    
        # Disc message
    def read(self, program_name):
        return self.kernel.read(program_name)
    
        # Memory manager message
    def get_free_base_address(self):
        return self.kernel.get_free_address() #SE DEBE CONSULTAR UN MEMORY MANAGER 
     
    def create_pcb(self):
        return self.kernel.create_pcb()
        
    def load(self, program):
        self.kernel.load(program)    
    
        # Memory manager message
    def load_in_memory(self, program, pcb):
        self.kernel.load_in_memory(program, pcb)
    
        # Pcb table message
    def get_last_pid(self):
        return self.kernel.get_last_pid()
    
        # Kernel Read from disc
    def read_program(self, program_name):
        return self.kernel.read(program_name)
    
    @property
    def is_process_running(self):
        return self.kernel.get_running_process() is not None
        
    # Device Getter
    def get_device_by_name(self, device_name):
        return self.kernel.get_device_by_name(device_name)
    
    def get_program_name_by_pid(self, pid):
        return self.kernel.get_program_name_by_pid(pid)
    
    def is_pcb_loaded(self, pid):
        return self.kernel.is_pcb_loaded(pid)
    
    def get_pcb_by_pid(self, pid):
        return self.kernel.get_pcb_by_pid(pid)