'''
Created on Jul 2, 2016

@author: santiago
'''

from main.interruptions.interruption_request import InterruptionRequest
from main.system.log.output import Output
from termcolor import colored
from random import randrange

class NewPaging(InterruptionRequest):
    
    def __init__(self, program_name, interruption_manager=None):
        InterruptionRequest.__init__(self, "NewInterruption")
        self.interruption_manager = interruption_manager
        self.program_name = program_name
        self.irq = "./"
        self.output = Output()
        self.pcb_to_add = None

    def execute(self):
        new_pcb = self.create_pcb()
        new_pcb.set_program_name(self.program_name)
            # Se busca una prioridad random entre 1 y 3 hasta parametrizar el scheduler
        priority = randrange(1, 4, 2)
        new_pcb.priority = priority
        
        # Valida si hay un proceso corriendo y decido quien entra en cpu
        if self.interruption_manager.is_process_running:
            self.interruption_manager.add_to_scheduler(new_pcb)
            self.pcb_to_add = self.interruption_manager.get_next_from_scheduler()
            self.interruption_manager.dispatcher_save_pcb(self.pcb_to_add)
        else:
            self.interruption_manager.dispatcher_save_pcb(new_pcb)
        self.log_process_creation(new_pcb)
        
    def create_pcb(self):    
        return self.interruption_manager.create_pcb()
        
    def read(self):
        self.program = self.interruption_manager.read(self.program_name)
            
    def load_in_memory(self):
        self.interruption_manager.load_in_memory(self.program) 

    # Log methods
    
    def log_process_creation(self, pcb):
        self.interruption_manager.output.log(colored("[ NEW INTERRUPTION ]", "green", attrs=["bold"]) + " >> process created with id: " + str(pcb.pid) + " and priority: " + str(pcb.priority))