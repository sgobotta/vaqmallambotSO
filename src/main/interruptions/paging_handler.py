'''
Created on Jul 2, 2016

@author: santiago
'''

from main.interruptions.kill import Kill
from main.interruptions.io_in import IOIn
from main.interruptions.new_paging import NewPaging
from main.interruptions.io_end import IOEnd
from main.interruptions.time_out import TimeOut
from main.interruptions.page_fault import PageFault

class PagingHandler:
    
    def __init__(self, interruption_manager=None):
        self.methods_map = {
                    "end": self.kill, 
                    "io": self.ioIn,
                    "./": self.new,
                    "out_io": self.ioEnd,
                    "time_out": self.timeOut,
                    "pagefault":self.pageFault
                    }
        self.interruption_manager = interruption_manager
        
    def set_interruption_manager(self,interruption):
        self.interruption_manager = interruption
        
    def handle(self, irq, interruption_manager):
        keys_list= self.methods_map.iterkeys()    
        for k in keys_list:
            if(irq.lower().startswith("./")):
                program_name = irq[2:].strip()
                result = self.methods_map["./"](program_name,interruption_manager)
            elif irq.lower().startswith(k):
                result = self.methods_map[k](irq,interruption_manager)
        
        return result

    def kill(self,irq, interruption_manager):
        return Kill(irq, interruption_manager)

    def ioIn(self, irq, interruption_manager):
        return IOIn(irq,interruption_manager)

    def new(self,program_name, interruption_manager):
        return NewPaging(program_name, interruption_manager)

    def ioEnd(self, irq, interruption_manager):
        return IOEnd(irq,interruption_manager)

    def timeOut(self, irq, interruption_manager):
        return TimeOut(irq, interruption_manager)
    
    def pageFault(self, irq, interruption_manager):
        return PageFault(irq, interruption_manager)
    