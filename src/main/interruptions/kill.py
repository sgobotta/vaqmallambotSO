from main.interruptions.interruption_request import InterruptionRequest
from main.strings.state import STATE
from termcolor import colored

class Kill(InterruptionRequest):
    
    def __init__(self, irq, interruption_manager=None):
        InterruptionRequest.__init__(self, "CPUInterruption")
        self.irq = irq
        self.interruption_manager = interruption_manager
        
    def execute(self):
        running_pcb = self.get_running_pcb()
        self.kill_pcb(running_pcb)
        self.interruption_manager.clean_cpu_registers()
        self.dispatch_new_pcb()
        
    # Execute methods
    
    def get_running_pcb(self):
        return self.interruption_manager.get_running_pcb()
    
    def kill_pcb(self, pcb):
        pcb.set_state(STATE.KILL)
        self.log_killing_process(pcb.id)
        self.interruption_manager.remove_from_pcb_table(pcb)
        
    def dispatch_new_pcb(self):    
        new_pcb = self.interruption_manager.get_next_from_scheduler()
        self.interruption_manager.dispatcher_save_pcb(new_pcb)
        
    # Log methods 
        
    def log_killing_process(self, pid):
        self.interruption_manager.output.log(colored("[ END INTERRUPTION ]", "red", attrs=["bold"]) + " >> Killing process with id: " + str(pid))