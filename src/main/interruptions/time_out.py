from main.interruptions.interruption_request import InterruptionRequest
from main.system.log.output import Output

class TimeOut(InterruptionRequest):
    def __init__(self, irq, interruption_manager=None):
        InterruptionRequest.__init__(self, "CPUInterruption")
        self.irq = irq
        self.interruption_manager = interruption_manager
        self.output = Output()
        
    def execute(self):
        self.interruption_manager.dispatcher_update()

        timed_out_pcb   = self.interruption_manager.get_pcb_from_dispatcher()
        ready_pcb       = self.interruption_manager.get_next_from_scheduler()

        self.interruption_manager.save_pcb(ready_pcb)
        self.interruption_manager.add_to_scheduler(timed_out_pcb)

        self.output.log("[ Instruction Timed Out ]")
