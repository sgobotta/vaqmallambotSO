'''
Created on Jul 2, 2016

@author: santiago
'''
from main.interruptions.interruption_request import InterruptionRequest

class PageFault(InterruptionRequest):
    
    def __init__(self, irq, interruption_manager=None):
        InterruptionRequest.__init__(self, "PageFault")
        self.irq = irq
        self.interruption_manager = interruption_manager
    
        self.pid        = None
        self.page       = None
        self.frame_size = None
        self.frame_id   = None
    
    def parse_irq(self):
        parsed_irq      = self.irq.split("_", 4)
        self.pid        = int(parsed_irq[1])
        self.page       = int(parsed_irq[2])
        self.frame_size = int(parsed_irq[3])
        self.frame_id   = int(parsed_irq[4])
    
    def execute(self):
        self.parse_irq()
        
        # Obtiene el nombre de programa mediante un pid
        program_name = self.interruption_manager.get_program_name_by_pid(self.pid)
        
        # Obtiene una lista de instrucciones
        instructions = self.interruption_manager.read_program(program_name)
        
        # Se corta la lista al tamagno de una pagina 
        start_list = self.page * self.frame_size
        end_list = start_list + self.frame_size
        instructions = instructions[start_list : end_list]
        
        pcb = self.interruption_manager.get_pcb_by_pid(self.pid)
        pcb.set_base_address(self.frame_id * self.frame_size)
        
        # El programa es cargado en memoria
        self.interruption_manager.load_in_memory(instructions, pcb)
        
        self.clean_registers()
        
    def clean_registers(self):
        self.pid        = None
        self.page       = None
        self.frame_size = None