from main.interruptions.interruption_request import InterruptionRequest
from main.strings.state import STATE
from termcolor import colored

class IOIn(InterruptionRequest):
    
    def __init__(self, irq, interruption_manager=None):
        InterruptionRequest.__init__(self, "CPUInterruption")
        self.irq = irq
        self.interruption_manager = interruption_manager
        
    def execute(self):
        self.interruption_manager.dispatcher_update()
        
        pcb    = self.get_pcb_from_dispatcher()
        device = self.get_device_from_irq()
        
        pcb.set_state(STATE.WAITING)
        self.interruption_manager.add_to_device_queue(pcb, device)
        self.log_process_enters_io(pcb)                
        pcb = self.interruption_manager.get_next_from_scheduler()
        self.interruption_manager.dispatcher_save_pcb(pcb)
  
    def get_device_from_irq(self):
        instruction = self.irq 
        device_name = instruction[3:].strip()
        return self.interruption_manager.get_device_by_name(device_name)

    def get_pcb_from_dispatcher(self):
        if not self.interruption_manager.is_dispatcher_empty():
            pcb = self.interruption_manager.get_pcb_from_dispatcher()
            pcb.state = STATE.WAITING
            return pcb
        else:
            self.log_dispatcher_empty_status()
            
    # Log methods
    def log_process_enters_io(self, pcb):
        self.interruption_manager.output.log(colored("[ IO IN INTERRUPTION ]", "green", attrs=["bold"]) + " >> process with id: " + str(pcb.pid) + " waits for IO")
        
    def log_dispatcher_empty_status(self):
        self.interruption_manager.output.log(colored("[ DISPATCHER ] >> is empty", "blue", attrs=["bold"]))