from main.interruptions.interruption_request import InterruptionRequest
from main.system.log.output import Output
from termcolor import colored

class IOEnd(InterruptionRequest):
    
    def __init__(self, irq, interruption_manager=None):
        InterruptionRequest.__init__(self, "IODeviceInterruption")
        self.irq                    = irq
        self.interruption_manager = interruption_manager
        self.output = Output()
        
    def execute(self):        
        device  = self.get_device_from_irq()
        pcb     = device.get_running()
        self.interruption_manager.add_to_scheduler(pcb)
        next_pcb = self.interruption_manager.get_next_from_scheduler()
        self.interruption_manager.dispatcher_save_pcb(next_pcb)

        irq = self.irq[7:].strip()
        self.log_ending_instruction(irq)
        
    def get_device_from_irq(self):
        instruction = self.irq 
        device_name = instruction[7:].strip()
        return self.interruption_manager.get_device_by_name(device_name)

    def get_next_from_device_queue(self, device):
        return self.interruption_manager.get_next_from_device_queue(device)
    
    def log_ending_instruction(self, irq):
        self.interruption_manager.output.log(colored("[ IO OUT INTERRUPTION ]", "red", attrs=["bold"]) + " >> out of i/o '" + irq + "'")