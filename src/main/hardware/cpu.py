from termcolor import colored
class CPU:
    
    def __init__(self, memory=None, interruption_manager=None, output=None, memory_manager=None):
        self.pc                     = -1
        self.pid                    = None
        self.instruction            = None
        self.interruption_manager   = interruption_manager
        self.memory_manager         = memory_manager
        self.output                 = output

    def set_memory_manager(self, memory_manager):
        self.memory_manager = memory_manager

    def set_interruption_manager(self, int_manager):
        self.interruption_manager = int_manager

    def set_memory(self, memory):
        self.memory = memory
        
    def set_output(self, output):
        self.output = output

    #Proposito:    calcular la nueva instruccion para luego evaluarla. Cpu prepara el pc
    #              para obtener la siguiente instruccion  
    #Precondicion: pc y base_adress deben ser un valor numerico      
    def fetch_instruction(self):
        self.instruction = self.memory_manager.get_instruction(self.pid, self.pc)
        
        self.evaluate()
        self.pc = self.pc + 1

    def evaluate(self):
        self.log_cpu_instruction()
        if self.interruption_manager.is_interruption_instruction(self.instruction):
            self.process_instruction()
                    
    def process_instruction(self):
        self.interruption_manager.handle(self.instruction)
    
    def process(self):
        if self.pid is not None:
            self.fetch_instruction()
        else:
            self.log_cpu_idle_status()

    # Log methods
    def log_cpu_instruction(self):
        self.output.log(colored("[ CPU ]", "cyan", attrs=["bold"])+ " >> " + self.instruction + " at pid: " + str(self.pid) + " and pc: " + str(self.pc))
            
    def log_cpu_idle_status(self):
        self.output.log(colored("[ CPU ]", "cyan", attrs=["bold"])+ " >> is idle")