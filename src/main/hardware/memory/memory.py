
class Memory:
    def __init__(self):
        self.ram = [None]*32
    
    def put(self, instruction, memory_address):
        self.ram[memory_address] = instruction

    def get(self, memory_address):
        return self.ram[memory_address]
    
    def move(self, instruction, memory_address):
        self.ram[memory_address] = instruction
