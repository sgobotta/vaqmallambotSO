'''
Created on Jun 18, 2016

@author: santiago
'''

class FullFramesTableException(Exception):

    def __init__(self):
        
        self.value = "Cannot add a new reference, frames table has no free slots"
        
    def __str__(self):
        return repr(self.value) 
        