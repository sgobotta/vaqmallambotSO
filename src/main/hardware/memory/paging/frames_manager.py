'''
Created on Jun 10, 2016

@author: santiago
'''

from main.hardware.memory.paging.exceptions import FullFramesTableException

'''
    FramesManager es una clase que administra una representacion de frames en memoria
    en una lista, que tiene un tamagno fijo, recibido por parametro. Se considera
    de manera abstracta que cada frame tiene un tamagno fijo, tambien recibido por parametro.
    La lista se compone de elementos que pueden ser ceros o unos, dependiendo si
    ese elemento esta siendo usado (1) o si el elemento esta libre (0) para su uso.
    De cada elemento importa conocer su posicion en la lista, es el dato que otras clases
    utilizaran para acceder a la tabla de frames y conocer aquellos que esten libres. 
'''

class FramesManager:

    def __init__(self, max_size=None, frame_size=None):
        
        self.frame_size = frame_size
        self.max_size   = max_size 
        self.table      = []
        
        self.generate_table()
    
    
    def generate_table(self):
        frames_limit = self.max_size
        while frames_limit > 0:
            self.table.append(0)
            frames_limit -= 1
    
    '''
        Getters
    '''
    def size(self):
        return self.max_size
        
    # Proposito:     Recorrer la lista hasta encontrar el indice del primer elemento no 
    #                utilizado (flag 0) y referenciarlo como utilizado.
    #
    # Precondicion:  Debe existir un frame libre
    #
    def get_free_frame(self):
        index = 0
        while self.table[index] != 0:
            index += 1
        self.add_reference_at(index)
        return index
        
    def get_base_address(self, index):
        return index * self.frame_size
    
    def get_frame_at(self, position):
        return self.table[position]
    
    def get_frame_position(self, frame):
        return self.table.index(frame)
    
    def used_slots(self):
        return self.count_slots_by_flag(1)
    
    def free_slots(self):
        return self.count_slots_by_flag(0)        
    
    # Flag es un entero 0 o 1
    #
    def count_slots_by_flag(self, flag):
        counter = 0
        for i in self.table:
            if i == flag:
                counter += 1
        return counter

    # Table has at least one free frame
    #
    def is_free_frame(self):
        return self.free_slots() > 0

    '''
        Frame reference
    '''
    
    # Sets a flag of 1 on a given index value of the frames list
    #
    def add_reference_at(self, index):
        if not self.is_free_frame():
            raise FullFramesTableException()
        else:
            self.table[index] = 1
            
        
    # Sets a flag of 0 on a given index value of the frames list
    #                
    def unreference_frame_at(self, index):
        self.table[index] = 0
        self.free_frame_index = index
