
'''
Created on Jun 2, 2016

@author: santiago
'''
from main.hardware.memory.paging.paging_table import PagingTable
from main.hardware.memory.paging.frames_manager import FramesManager
from main.hardware.memory.paging.paging_manager import PagingTableManager
from main.hardware.memory.paging.swap_manager import SwapManager
from random import randrange
from termcolor import colored

class Paging:
    
    def __init__(self, frame_size=None, mem_size=None, policy=None, loader=None, interruption_manager=None, output=None):
        
        self.loader               = loader 
        
        self.frame_size           = frame_size
        self.paging_table         = PagingTable()
        self.paging_table_manager = PagingTableManager(self.paging_table)
        self.swap_manager         = SwapManager(mem_size, frame_size)
        
        self.frames_table         = FramesManager(mem_size, frame_size)
        
        self.policy               = policy
        
        self.interruption_manager = interruption_manager

        self.output = output

    def set_policy(self, policy):
        self.policy = policy

    def set_interruption_manager(self, interruption_manager):
        self.interruption_manager = interruption_manager

    def set_loader(self, loader):
        self.loader = loader

    def set_output(self, output):
        self.output = output

    '''
            Instruction getters
    '''
    def get_page_number(self, pc):
        return pc / self.frame_size
    
    def get_instruction_address(self, pc):
        return pc % self.frame_size


    def get_base_address(self, pid, pc):
        
        expected_page = self.get_page_number(pc)
        self.paging_table_manager.add_to_table(pid, expected_page, self.policy.get_policy_usage_reference())
        base_address = self.get_base_address_by_pid(pid, expected_page) + self.get_instruction_address(pc)

        return base_address


    # Proposito:    De acuerdo al estado en una tabla de referencias para un pid y page dados, retornara
    #               una base_address que referencia a una direccion en memoria
    #
    # Precondicion: page ya debe estar referenciada a pid en la tabla de referencias (get_instruction())
    #
    def get_base_address_by_pid(self, pid, page):
        
        if self.paging_table_manager.is_page_loaded(pid, page):
            
                # Paging Table Manager >> Devuelve el frame asociado con el pid y page dados
            frame_id = self.paging_table_manager.get_frame_number_by(pid, page)
            self.policy.on_get_free_frame_action(pid, page, frame_id)
                # Frames Table >> Devuelve la base address asociada a esa posicion de frame
            return self.frames_table.get_base_address(frame_id) 
        
        if self.paging_table_manager.is_page_swap_referenced(pid, page):
            
            self.log_page_fault_memory(pid, page)

            frame_id = self.get_free_frame()
            swap_reference = self.paging_table_manager.get_swap_reference(pid, page)

            # Loader carga en memoria las instrucciones que devuelvo (entiendo que en swap
            # se almacena la pagina entera con las instrucciones, por lo que no hay que
            # ir a disco)
            
                # Deberia pasarle un loader para que cargue el programa, por ahora
                # solamente actualiza la paging table mediante el manager
            self.swap_manager.swap_out(swap_reference, self.paging_table_manager, pid, page, frame_id)

            frame_id = self.get_frame_number_by(pid, page)
            self.policy.on_get_free_frame_action(pid, page, frame_id)
            #return self.frames_table.get_base_address(frame_id)
            return self.get_base_address_by_pid(pid, page)

        else:
            self.log_page_fault_swap(pid, page)
            
            free_frame_id = self.get_free_frame()

            # Loader carga en memoria las instrucciones durante una page fault
            params = "_" + str(pid) + "_" + str(page) + "_" + str(self.frame_size) + "_" + str(free_frame_id)
            irq = "pagefault" + params
            
            self.interruption_manager.handle(irq)

            self.paging_table_manager.load_page_with_frame(pid, page, free_frame_id)
            expected_frame = self.get_frame_number_by(pid, page)

            #return self.frames_table.get_base_address(expected_frame)
            return self.get_base_address_by_pid(pid, page)
        

    # # # # # # #
    # Proposito: Dado un pid y page retorna el numero de frame libre en
    #            una tabla de frames.
    # 
    def get_frame_number_by(self, pid, page):
        expected_frame = self.paging_table_manager.get_frame_number_by(pid, page)
        if expected_frame < 0:
            raise Exception("Frame no esta cargado en la posicion dada")
        self.policy.on_get_free_frame_action(pid, page, expected_frame)
        return expected_frame

    # Proposito: Retornar el indice de un frame elegido aleatoriamente
    def get_random_victim(self):
        return randrange(0, self.frames_table.max_size) 

    def get_free_frame(self):
        if self.frames_table.is_free_frame():
            return self.frames_table.get_free_frame()
        else:
            #return self.get_random_victim()
            return self.policy.get_free_frame()
        

    '''
            Loaders
    '''
    # El algoritmo solamente es llamado, cuando paging_strategy ya sabe que
    # no hay frames libres.
    # La policy buscara un frame "victima", lo retornara y desde esta strategy, el
    # table manager sabra que cambios hacer: borrar la referencia de este frame 
    # nuevo en el     # pid-page en el que estaba, swapearlo y borrar la referencia
    # de frame que tenia, referenciar ese frame a un nuevo pid-page, 
    # poner el load en 1
    

    def load_page(self, pid, page):
        #Un algoritmo elige frame libre y lo aniade
        free_frame = self.policy.get_free_frame()
        self.policy.add_frame(self.frames_table, free_frame)

        #Se obtiene el id del frame libre
        frame_number = self.frames_table.get_frame_position(free_frame)

        #Carga un frame y cambia el estado load
        self.paging_table_manager.load_page_with_frame(pid, page, frame_number)

    def strategy_load(self, program, pcb):
        self.loader.load(program)

    def kill_process(self, pcb):
        pass

    '''
        Log methods
    '''
    
    def print_memory(self):
        self.printPagingTable()
        #self.printFrames()

    def printPagingTable(self):
        paging_table = self.paging_table_manager.paging_table
        print "\t"
        print colored("PAGING TABLE", attrs=["bold"])     
        print "__________________________________________________________________________\n"
        print colored("PID \t PAGE \t FRAME \t LOAD \t SWAP \t USAGE", "white", attrs=["bold"])
        print " " 
        
        pids   = paging_table.get_pid_list()
        pages  = paging_table.get_page_list()
        frames = paging_table.get_frame_list()
        load   = paging_table.get_load_list()
        swap   = paging_table.get_swap_list()
        usage  = paging_table.get_usage_list()
        x = 0
        for i in paging_table.get_pid_list():
            print str(pids[x]) + " \t " + str(pages[x]) + " \t " + str(frames[x]) + " \t " + str(load[x]) + " \t" + str(swap[x]) + " \t " + str(usage[x])
            x = x + 1
        print " "
        
    def printFrames(self):
        frames_table = self.frames_table.table
        max_size     = self.frames_table.max_size
        frame_size   = self.frames_table.frame_size
        
        print "\t"
        print "__________________________________________________________________________"
        print colored("FRAMES TABLE \t MAX SIZE: " +  str(max_size) + " \t FRAME SIZE: " + str(frame_size), attrs=["bold"]) 
        print frames_table
        
    def log_page_fault_memory(self, pid, page):
        self.output.log(colored("[ PAGE FAULT ]", "red", attrs=["bold"]) + " >> failed to fetch page " + str(page) + " for pid " + str(pid) + " from memory")

    def log_page_fault_swap(self, pid, page):
        self.output.log(colored("[ PAGE FAULT ]", "red", attrs=["bold"]) + " >> failed to fetch page " + str(page) + " for pid " + str(pid) + " from swap")      