'''
Created on Jun 18, 2016

@author: santiago
'''

from random import randrange
from termcolor import colored

class SwapManager:
    
    def __init__(self, memory_size, frame_size, interruption_manager=None, loader=None, output=None):
        
        self.swap_memory          = []
        self.frame_size           = frame_size
        self.interruption_manager = interruption_manager
        self.laoder               = loader
        self.output               = output
        
        self.generate_swap(memory_size)
    
    def set_interruption_manager(self, interruption_manager):
        self.interruption_manager = interruption_manager
    
    def set_loader(self, loader):
        self.loader = loader
    
    def set_output(self, output):
        self.output = output
    
    def generate_swap(self, memory_size):
        swap_size = memory_size * 2
        while swap_size > 0:
            self.swap_memory.append(-1)
            swap_size -= 1
    
    def swap_out(self, swap_index, paging_table_manager, pid_index, page_index, frame_index):
        
        # retorna un diccionario con las instrucciones del programa
        expected_page_instructions = self.swap_memory[swap_index]
        
        # carga las isntrucciones de la pagina en memoria mediante un loader
        self.loader.load(expected_page_instructions)
        
        # Actualiza la tabla de paginacion
        paging_table_manager.load_page_from_swap_at_frame(pid_index, page_index, frame_index)
        
        # Elimina la pagina de la memoria swap
        self.swap_memory[swap_index] = -1
        
        self.log_program_fetched_from_swap(expected_page_instructions, swap_index)
        
    def swap_in(self, paging_table_manager, pid, page):
        
        if self.interruption_manager.is_pcb_loaded(pid):
            program_name = self.interruption_manager.get_program_name_by_pid(pid)
            
            program = self.loader.read(program_name)
            
            start_list = int(page) * int(self.frame_size)
            end_list = start_list + int(self.frame_size)
            program = program[start_list : end_list]
            
            free_swap_slot_index = self.get_free_slot()
            
            self.swap_memory[free_swap_slot_index] = program
            self.log_program_loaded_in_swap(program, free_swap_slot_index)
            
            paging_table_manager.set_swap_reference(pid, page, free_swap_slot_index)
        else:
            self.log_couldnt_load_in_swap()
        
    def get_free_slot(self):
        index = 0
        free_slot = None
        for slot in self.swap_memory:
            if slot == -1:
                free_slot = slot
            index = index + 1
        if index >= len(self.swap_memory):
            return randrange(0, len(self.swap_memory))
        else:
            return index 
            
        
    def store_instructions(self, index, program):
        self.swap_memory[index] = program


    def log_program_loaded_in_swap(self, program, index):
        self.output.log(colored("[ SWAP MANAGER ]", "yellow", attrs=["bold"]) + " >> Program: " + str(program) + " succesfully loaded in swap at index: " + str(index))

    def log_program_fetched_from_swap(self, program, index):
        self.output.log(colored("[ SWAP MANAGER ]", "yellow", attrs=["bold"]) + " >> Program: " + str(program) + " succesfully fetched from swap at index: " + str(index))

    def log_couldnt_load_in_swap(self):
        self.output.log(colored("[ SWAP MANAGER ]", "yellow", attrs=["bold"]) + " >> Couldnt fetch program name, pcb is not in pcb table")