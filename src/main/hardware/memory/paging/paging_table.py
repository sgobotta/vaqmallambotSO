'''
Created on Jun 7, 2016

@author: santiago
'''

class PagingTable:
    
    def __init__(self):
        
        self.table = self.create_empty_matrix(6)
        
        # +---+----------+------------------------------------------------------------+
        # | n | name     |  description                                               |
        # +---+----------+------------------------------------------------------------+
        # | 0 | pid      |  stores the pid id                                         |
        # | 1 | page     |  stores the page nuumber                                   |
        # | 2 | frame    |  stores the frame position                                 |
        # | 3 | isLoaded |  1 if has frame referenced, 0 if has no frame referenced   |
        # | 4 | swap     |  stores the swap position, -1 if has no swap reference     |
        # | 5 | usage    |  list used for algorithm's statistic purposes              |
        # +---+----------+------------------------------------------------------------+
    def create_empty_matrix(self, number_of_columns):
        return [range(0) for i in range(number_of_columns)]
    
    
    '''
        Getters
    '''
    
    def get_pid_list(self):
        return self.table.__getitem__(0)
    
    def get_page_list(self):
        return self.table.__getitem__(1)
    
    def get_frame_list(self):
        return self.table.__getitem__(2)
    
    def get_load_list(self):
        return self.table.__getitem__(3)

    def get_swap_list(self):
        return self.table.__getitem__(4)
    
    def get_usage_list(self):
        return self.table.__getitem__(5)
    

    '''
        Setters
    '''
    
    # Proposito:    asigna el valor 1 (loaded) en la posicion load_index de la lista de load 
    #
    # Precondicion: elemento en indice load_index debe existir
    #
    def set_load_at(self, load_index):
        self.get_load_list()[load_index] = 1
        
    # Proposito:    asigna el valor 0 (not loaded) en la posicion load_index de la lista de load 
    #
    # Precondicion: elemento en indice load_index debe existir
    #
    def unload_at(self, load_index):
        self.get_load_list()[load_index] = 0

    # Proposito:    asigna el valor frame_id en la posicion frame_index de la lista de frames 
    #
    # Precondicion: elemento en indice frame_index debe existir
    #
    def set_frame_at(self, frame_index, frame_id):
        self.get_frame_list()[frame_index] = frame_id

    # Proposito:    asigna el valor swap_index en la posicion index de la lista de referencias de swap memory 
    #
    # Precondicion: elemento en indice index debe existir
    #
    def set_swap_reference_at(self, swap_index, reference):
        self.get_swap_list()[swap_index] = reference
        

    # Proposito:    asigna el valor -1 (not swap referenced) en la posicion swap_index de la lista de referencias de swap memory 
    #
    # Precondicion: elemento en indice index debe existir
    #
    def remove_swap_reference_at(self, swap_index):
        self.get_swap_list()[swap_index] = -1

    # Dados pid y page, los inicializa en la tabla con los valores default 
    def add_reference(self, pid, page, policy_usage_reference):
        self.get_pid_list()   .append(pid)
        self.get_page_list()  .append(page)
        self.get_frame_list() .append(-1)
        self.get_load_list()  .append( 0)
        self.get_swap_list()  .append(-1)
        self.get_usage_list() .append(policy_usage_reference)

    # Dado un indice, se inserta el valor 'reference_value' en la posicion
    # correspondiente de una lista 'usage_list', utilizada para estadisticas
    # particulares para cada politica de eliminacion de la estrategia
    # de paginacion
    def set_usage_reference_at(self, frame, reference_value):
        index = self.get_frame_list().index(frame)
        self.get_usage_list()[index] = reference_value
        
