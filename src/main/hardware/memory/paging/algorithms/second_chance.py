'''
Created on 16 de jun. de 2016

@author: ignacio, santiago
'''

from main.datastructures.queue import Queue
from main.hardware.memory.paging import paging_manager, swap_manager
from termcolor import colored

'''
    The Second Change Replacement Algorithm es una de las politicas utilizadas
    para la seleccion de frames a utilizar ante pedidos durante el manejo de 
    memoria. En una tabla de paginacion se mantiene referencia de todas las paginas
    y frames que se utilizan junto con un estado para cada uno de ellos que influenciara
    en la desicion a tomar sobre este frame. La referencia es un bit en 1 (si todavia
    no fue referenciado en ningun momento) o 0 (el frame ya ha sido referenciado y su bit
    en 1 cambio a 0). El primer frame encontrado con un bit en 0 (cero) sera aquel
    que se sobreescribira

'''

class SecondChance:
     
    def __init__(self, table_manager=None, output=None, swap_manager=None):
        
        self.table_manager = paging_manager
        self.frames_queue  = Queue()
        self.swap_manager  = swap_manager
        
        self.output        = output
   
    def set_table_manager(self, table_manager):
        self.table_manager = table_manager
   
    def set_swap_manager(self, swap_manager):
        self.swap_manager = swap_manager
        
    def set_output(self, output):
        self.output = output
        
    # Second chance utiliza unda estadisica de bit read. Se marca como 1 cada vez que se referencia
    # un valor en la tabla de paginacion
    def get_policy_usage_reference(self):
        return 1        
        
    # Proposito: agrega un nuevo frame a la cola interna Fifo
    #        
    def add_new_frame(self, free_frame_number):
        self.frames_queue.queue(free_frame_number)

    # Proposito: se recorre la lista de frames, verificando cada vez su bit de read,
    # asignando un 0 (cero) en caso de ser distinto a 0 (cero), hasta encontrar el primero con 
    # valor 0 (cero), el cual se eligira como victima.
    # 
    def get_free_frame(self):
        frame_id = self.frames_queue.dequeue()
        victim = None
        
        usage_list = self.table_manager.get_usage_list()
        load_list  = self.table_manager.get_load_list()
        
        i = 0
        while victim is None:
            if load_list[i] == 1 and usage_list[i] != 0:
                usage_list[i] = 0
            else:
                victim = frame_id
            
            self.frames_queue.queue(frame_id)
            frame_id = self.frames_queue.dequeue()
            i = i + 1
        self.on_replacement_action_do_at(frame_id)
        self.log_get_free_frame(frame_id)
        return frame_id

    # Dado un frame a reemplazar, se realiza swap in y se marca el pid y page
    # correspondiente como unloaded
    def on_replacement_action_do_at(self, frame_id):
        pid  = self.table_manager.get_pid_by_loaded_frame(frame_id)
        page = self.table_manager.get_page_by_loaded_frame(frame_id)
        load_index = self.table_manager.get_page_index(pid, page)
        
        self.swap_manager.swap_in(self.table_manager, pid, page)
        self.table_manager.unload_at(load_index)
    

    # Proposito: Dado un pid y una page, conoce el numero de frame proximo
    # a agregar a la cola interna Fifo
    def on_get_free_frame_action(self, pid, page, frame):
        free_frame = self.table_manager.get_frame_number_by(pid, page)
        self.add_new_frame(free_frame)

    def log_get_free_frame(self, free_frame):
        self.output.log(colored("[ SECOND CHANCE ]", "magenta", attrs=["bold"]) + " >> Found a free frame with number '" + str(free_frame) + "'")
