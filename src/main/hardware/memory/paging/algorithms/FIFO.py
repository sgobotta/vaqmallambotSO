'''
Created on 16 de jun. de 2016

@author: ignacio
'''

from main.datastructures.queue import Queue
from termcolor import colored


'''
    First in First out es una de las politicas utilizadas para seleccionar
    un frame libre dentro de una cola de frames disponibles en la memoria 
    logica. Cada vez que se utiliza un frame para un pid y page dados
    se mantiene referencia del mismo en una cola fifo.

'''
class Fifo:
    
    def __init__(self, table_manager=None, output=None, swap_manager=None):
        self.frames_queue  = Queue()
        self.table_manager = table_manager
        self.swap_manager  = swap_manager
        
        self.output        = output
    def set_table_manager(self, table_manager):
        self.table_manager = table_manager
    
    def set_swap_manager(self, swap_manager):
        self.swap_manager = swap_manager
    
    # Default usage reference
    def get_policy_usage_reference(self):
        return -1
    
    def set_output(self, output):
        self.output = output
    
    # Proposito: agrega un nuevo frame a la cola interna Fifo
    #
    def add_new_frame(self, frame):
        self.frames_queue.queue(frame)
        
    # Proposito: retorna el siguiente frame de la cola interna Fifo
    #
    def get_free_frame(self):
        head_frame = self.frames_queue.dequeue()
        self.on_replacement_action_do_at(head_frame)
        self.log_get_free_frame(head_frame)
        return head_frame
    
    def on_replacement_action_do_at(self, frame_id):
        pid  = self.table_manager.get_pid_by_loaded_frame(frame_id)
        page = self.table_manager.get_page_by_loaded_frame(frame_id)
        load_index = self.table_manager.get_page_index(pid, page)
        
        self.swap_manager.swap_in(self.table_manager, pid, page)
        self.table_manager.unload_at(load_index)
    
    # Proposito: Dado un pid y una page, conoce el numero de frame proximo
    # a agregar a la cola interna Fifo
    def on_get_free_frame_action(self, pid, page, frame_id):
        free_frame_number = self.table_manager.get_frame_number_by(pid, page)
        self.add_new_frame(free_frame_number)
     
    def log_get_free_frame(self, head_frame):
        self.output.log(colored("[ FIFO ]", "magenta", attrs=["bold"]) + " >> Found a free frame with number '" + str(head_frame) + "'")