'''
Created on Jun 28, 2016

@author: santiago
'''
from datetime import datetime
from termcolor import colored

'''

    Least Recently Used es una politica utilizada para seleccionar un frame
    determinado de la memoria logica. Asume la existencia de una tabla de 
    paginacion con una propiedad (o columna) de estadisticas utilizada
    especialmente para el algoritmo utilizado en la estrategia de paginacion.

'''

class LRU:
    
    def __init__(self, table_manager=None, swap_manager=None, output=None, timestamp_provider=datetime):
        
        self.table_manager      = table_manager
        self.swap_manager       = swap_manager
        self.timestamp_provider = timestamp_provider
        
        self.output             = output

    def set_table_manager(self, table_manager):
        self.table_manager = table_manager

    def set_swap_manager(self, swap_manager):
        self.swap_manager = swap_manager
        
    def set_output(self, output):
        self.output = output

    # Default usage reference    
    def get_policy_usage_reference(self):
        return datetime.now()
    
    # Proposito: retornar el indice del elemento con la referencia mas antigua
    #            sobre la lista de indices de aquellos pid que estan en estado load
    #
    # Precondicion: la lista de elementos en estado load no debe estar vacia      
    def get_free_frame(self):
        loaded_indexes = self.table_manager.get_loaded_indexes()
        oldest_reference_index = self.find_oldest_usage_index(loaded_indexes)
        
        if oldest_reference_index is not None:
            self.log_get_free_frame(oldest_reference_index)
            return oldest_reference_index
            #return self.table_manager.get_frame_list()[oldest_reference
            
            
    # Proposito: recorrer los elementos de la tabla de paginacion para encontrar
    #            aquel con la estadistica de uso mas baja entre todos
    def find_oldest_usage_index(self, index_list):
        
        date             = datetime.now()
        oldest_reference = None
        usage_list       = self.table_manager.get_usage_list()
        for index in index_list:
            if usage_list[index] < date:
                date = usage_list[index]
                oldest_reference = index
        self.on_replacement_action_do_at(oldest_reference)
        return oldest_reference
    
    # Dado un frame a reemplazar, se realiza swap in y se marca el pid y page
    # correspondiente como unloaded
    def on_replacement_action_do_at(self, replaced_index):
        pid  = self.table_manager.get_pid_list()[replaced_index]
        page = self.table_manager.get_page_list()[replaced_index]
        
        self.swap_manager.swap_in(self.table_manager, pid, page)
        self.table_manager.unload_at(replaced_index)
        
    
    # Actualiza la estadistica de uso a su valor default para una page de un pid en particular con
    # un timestamp
    #
    def on_get_free_frame_action(self, pid, page, frame):
        time = self.timestamp_provider.now()
        self.table_manager.set_usage_reference(frame, time)

    def log_get_free_frame(self, oldest_reference_index):
        self.output.log(colored("[ LRU ]", "magenta", attrs=["bold"]) + " >> Found a free frame with number '" + str(oldest_reference_index) + "'")
