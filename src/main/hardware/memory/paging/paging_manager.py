'''
Created on Jun 11, 2016

@author: santiago
'''

class PagingTableManager:
    
    def __init__(self, paging_table=None):
        
        self.paging_table = paging_table

    '''
        Boolean Getters
    '''   
    # Dado un entero, retorna un booleano indicando si el pid esta referenciado
    def is_pid_referenced(self, pid):
        return pid in self.paging_table.get_pid_list()
    
    # Dados dos enteros, retorna un booleano indicando si la pagina 'page' esta en la tabla
    def is_page_referenced(self, pid, page):        
        is_referenced = False
        i = 0
        page_list = self.paging_table.get_page_list()
        pid_indexes_list = self.get_pid_indexes(pid)
        
        while i < len(pid_indexes_list) and is_referenced != True:
            if page == page_list[pid_indexes_list[i]]:
                is_referenced = True
            i = i + 1
        return is_referenced
    
    # Dados dos enteros, retorna un booleano indicando si la instruccion esta cargada
    def is_instruction_loaded(self, pid, page):
        return self.is_pid_referenced(pid) and self.is_page_loaded(pid, page)
    
    # Dados dos enteros, retorna un booleano indicando si la pagina 'page' esta en estado load
    def is_page_loaded(self, pid, page):      
        page_index = self.get_page_index(pid, page)
        return self.paging_table.get_load_list()[page_index] == 1

    # Dados dos enteros, retorna un booleano indicando si la pagina no esta load y si se
    # encuentra cargada en la memoria swap    
    def is_page_swap_referenced(self, pid, page):
        page_index = self.get_page_index(pid, page)
        return self.paging_table.get_swap_list()[page_index] != -1

    '''
            Getters
    '''
        
    # Proposito:     Dado un pid, retorna sus posiciones en la lista de pids
    # Precondicion:  pid esta referenciado en la lista de pids    
    def get_pid_indexes(self, pid):
        pid_list    = self.paging_table.get_pid_list()
        pid_indexes = []
        i = 0
        for p in pid_list:
            if p == pid:
                pid_indexes.append(i)
            i = i + 1
        return pid_indexes
            
    # Proposito:     Dado un pid y una pagina, retorna su posicion en una lista
    # Precondicion:  Pid y pagina deben existir en la tabla        
    def get_page_index(self, pid, page):
        pid_indexes = self.get_pid_indexes(pid)
        page_index  = None
        i = 0
        for index in pid_indexes:
            if self.paging_table.get_page_list()[index] == page:
                page_index = index
            i = i + 1
        return page_index

    # Dado un pid y page se obtiene el valor representado en la lista de frame
    def get_frame_number_by(self, pid, page):
        index = self.get_page_index(pid, page)
        return self.paging_table.get_frame_list()[index]
 
    def get_pid_by_loaded_frame(self, frame_id):
        frames_list = self.paging_table.get_frame_list()
        load_list   = self.paging_table.get_load_list()
        
        frame_index = None
         
        i = 0
        for frame in frames_list:
            if frame == frame_id and load_list[i] == 1:
                frame_index = i
            i = i + 1
        
        return self.paging_table.get_pid_list()[frame_index]
 
    def get_page_by_loaded_frame(self, frame_id):
        frames_list = self.paging_table.get_frame_list()
        load_list   = self.paging_table.get_load_list()
        
        frame_index = None
         
        i = 0
        for frame in frames_list:
            if frame == frame_id and load_list[i] == 1:
                frame_index = i
            i = i + 1
        
        return self.paging_table.get_page_list()[frame_index]        

    # Dado un pid y page, se obtiene el valor referenciado a su lista de swap 
    def get_swap_reference(self, pid, page):
        index = self.get_page_index(pid, page)
        return self.paging_table.get_swap_list()[index]
    
    # Para un pid y page, una tabla de referencias asigna una referencia a swap 'swap_reference'
    def set_swap_reference(self, pid, page, swap_reference):
        index = self.get_page_index(pid, page)
        self.paging_table.set_swap_reference_at(index, swap_reference)
 
    # Proposito:    Retorna una lista con los indices de aquellos elementos
    #               que estan referenciados como loaded, es decir, que 
    #               estan cargados en memoria y su valor en la lista load es 1. 
    def get_loaded_indexes(self):
        load_list      = self.paging_table.get_load_list()
        indexes_list  = []
        i = 0
        for value in load_list:
            if value == 1:
                indexes_list.append(i)
            i = i + 1                
        return indexes_list
 
    '''
            Paging table access
    '''

    # Para un pid y un frame, una tabla de referencias asigna un frame y marca
    # la referencia de load
    def load_page_with_frame(self, pid, page, frame_index):
        page_index = self.get_page_index(pid, page)
        self.paging_table.set_frame_at(page_index, frame_index)
        self.paging_table.set_load_at(page_index)

    # Dado un pid, page, y un valor estadistico de uso, una tabla de referencias
    # agnade una nueva referencia
    def add_reference(self, pid, page, policy_usage_reference):
        self.paging_table.add_reference(pid, page, policy_usage_reference)
        
    # Agrega una referencia a una pagina page para un id pid, solo si
    # no existia tal referencia en la tabla de paginacion 
    def add_to_table(self, pid, page, policy_usage_reference):
        if not self.is_page_referenced(pid, page):
            self.add_reference(pid, page, policy_usage_reference)

    # Dado un pid, page y frame_index, se actualiza la tabla al generarse un swap_out
    def load_page_from_swap_at_frame(self, pid, page, frame_index):
        page_index = self.get_page_index(pid, page)
        self.paging_table.set_frame_at(page_index, frame_index)
        self.paging_table.set_load_at(page_index)
        self.paging_table.remove_swap_reference_at(page_index)

    # Dado un pid y page, actualiza el valor en la posicion de la propiedad
    # o columna 'usage' que le corresponde, con la referencia dada  
    def set_usage_reference(self, frame, reference_value):
        self.paging_table.set_usage_reference_at(frame, reference_value)

    # Dado un numero que representa un frame, se obtiene el valor representado
    # en la lista de estadisticas de uso
    def get_usage_value_by_frame(self, frame_number):
        index = self.paging_table.get_frame_list().index(frame_number)
        return self.paging_table.get_usage_list()[index]
    
    # Retorna la lista de pid de la tabla de paginacion
    def get_pid_list(self):
        return self.paging_table.get_pid_list()
    
    # Retorna la lista de pagina de la tabla de paginacion
    def get_page_list(self):
        return self.paging_table.get_page_list()
    
    # Retorna la lista de frames de la tabla de paginacion
    def get_frame_list(self):
        return self.paging_table.get_frame_list()
    
    # Retorna la lista de load de la tabla de paginacion
    def get_load_list(self):
        return self.paging_table.get_load_list()
    
    # Retorna la lista de usos de la tabla de paginacion
    def get_usage_list(self):
        return self.paging_table.get_usage_list()
    
    # Indica sobre que indice la tabla de paginacion debe realizar unload_at
    def unload_at(self, index):
        self.paging_table.unload_at(index)