'''
Created on 2/7/2016

@author: leonardo
'''
from termcolor import colored

class StaticMemory:
    
    def __init__(self, kernel=None, memory_manager=None):
        self.kernel = kernel
        self.memory_manager = memory_manager
        
    def get_base_address(self, pid, pc):
        return self.kernel.get_base_address(pid) + pc
        
    def set_kernel(self, kernel):
        self.kernel = kernel
        
    def set_memory_manager(self, memory_manager):
        self.memory_manager = memory_manager
        
    def strategy_load(self, program, pcb):
        self.memory_manager.load(program)
        
    def print_memory(self):
        print colored("RAM: ", attrs=['bold'])+ "\n" + str(self.memory_manager.loader.memory.ram)
        
    # Mensaje general que reciben todas las estrategias    
    def kill_process(self, pcb):
        pass