'''
Created on 19 de may. de 2016

@author: gabriel, Ignacio, santiago
'''
from termcolor import colored

class MemoryManager:
    
    def __init__(self, allocation_strategy=None, output=None, loader=None, kernel=None):
        self.allocation_strategy = allocation_strategy
        self.loader = loader
        self.kernel = kernel
        self.output = output
        
    def set_kernel(self, kernel):
        self.kernel = kernel
    
    def set_loader(self, loader):
        self.loader = loader
    
    def store_pcb(self, pcb):
        self.allocation_strategy.store_pcb(pcb)    
    
    def set_allocation_strategy(self, allocation_strategy):
        self.allocation_strategy = allocation_strategy
        
    def set_output(self, output):
        self.output = output
        
    def get_free_address(self):
        return self.loader.get_free_address()
    
    def get_pcb_by_pid(self, pid):
        return self.kernel.get_pcb_by_pid(pid)
        
    def load(self, program):
        self.loader.load(program)

    def strategy_load(self, program, pcb): 
        self.free_address = pcb.get_base_address()
        self.loader.set_base_address(pcb.get_base_address()) 
        self.allocation_strategy.strategy_load(program, pcb)   

    def get_instruction(self, pid, pc):
        self.loader.set_base_address(self.allocation_strategy.get_base_address(pid, pc))
        return self.loader.get_instruction()
        
    def move_instructions(self, number_of_spaces, start, end):
        self.loader.move_instructions(number_of_spaces, start, end)
        
    def kill_process(self, pcb):
        self.allocation_strategy.kill_process(pcb)
        
    def set_loader_base_address(self, addr):
        self.loader.set_base_address(addr)
            
    def log_instruction_loaded(self, instruction):
        self.output.log(colored("[ MEMORY MANAGER ]", "yellow", attrs=["bold"]) + " >> trying to load '" + instruction + "' in address: [" + str(self.loader.base_address) + "]")
