'''
Created on Jun 2, 2016

@author: santiago
'''
from main.hardware.memory.dynamic_relocation.BSODException import BSODException

class BestFit:
    
    def __init__(self, relocation_register=None):
        self.relocation_register = relocation_register
        
        self.best_offset         = None
        self.best_register       = None
        
    def alloc(self, program, pcb):
        if (self.relocation_register.get_free_size() < len(program) ):
            raise BSODException()
        else:
            block = self.search_free_block(program)
            if(block is None):
                self.relocation_register.compact() 
                block = self.search_free_block(program)
                
            self.relocation_register.assign_to_block(program, pcb, block)

    # Dado un programa, recorre una lista de bloques libres para enconrtrar aquel cuyo
    # tamanio es el que mas se ajusta al tamanho del programa
    def search_free_block(self, program):
        blocks = self.get_free_blocks()
        i = 0
        current_block = None
        self.best_offset = blocks[i].get_size() 
        while i < len(blocks):
            potential_block = blocks[i]
            self.evaluate_blocks(current_block, potential_block, len(program))
            i += 1
        return self.best_register

    # Dados dos registros y un tamanho de programa, se evalua una condicion para almacenar uno
    # de esos registros en best_register
    def evaluate_blocks(self, current_register, potential_register, program_size):
        new_offset = self.calculate_offset(potential_register.get_size(), program_size)
        if new_offset >= 0 and new_offset < self.best_offset:
            self.best_offset   = new_offset
            self.best_register = potential_register

    # Dados dos tamanhos de registro, se obtiene la diferencia    
    def calculate_offset(self, register_size, program_size):
        return register_size - program_size

    # Retorna una lista de bloques vacios        
    def get_free_blocks(self):
        return self.relocation_register.get_free_blocks()
    
    def set_relocation_register(self, reloc):
        self.relocation_register = reloc
