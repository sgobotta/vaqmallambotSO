'''
Created on 2/6/2016

@author: leonardo
'''
from main.hardware.memory.dynamic_relocation.BSODException import BSODException

class WorstFit:
    
    def __init__(self, relocation_register=None):
        self.relocation_register = relocation_register
                   
    def alloc(self, program, pcb):
        if (self.relocation_register.get_free_size() < len(program) ):
            raise BSODException()
        else:
            block = self.search_free_block(program)
            if(block is None):
                self.relocation_register.compact() 
                block = self.search_free_block(program)
                
            self.relocation_register.assign_to_block(program, pcb, block)
            
    def assing(self, block, program, pcb):
        if block is not None:
            self.relocation_register.assign_to_block(program, pcb, block)
        else:
            self.relocation_register.compact()
    
            
    def search_free_block(self, program):
        
        blocks = self.get_free_blocks()
        current_block = blocks[0]
    
        for block in blocks:
            if current_block.get_size() < block.get_size():
                current_block = block
            
        if current_block.get_size() < len(program):
            current_block = None            

        return current_block
    
    
    def evaluate_blocks(self, current_register, potential_register, current_offset, program_size):
        
        new_offset = self.calculate_offset(potential_register.get_size(), program_size) 
        if new_offset < current_offset and new_offset >= 0:
            return potential_register
        else:
            return current_register
           
    
    def calculate_offset(self, register_size, program_size):
        return register_size - program_size
        
    def get_free_blocks(self):
        return self.relocation_register.get_free_blocks()
    
    def set_relocation_register(self, reloc):
        self.relocation_register = reloc
        
    