'''

@author: gabriel
'''
from main.hardware.memory.dynamic_relocation.BSODException import BSODException


class FirstFit:
    
    def __init__(self, relocation_register=None):
        self.relocation_register = relocation_register
    
    def alloc(self, program, pcb):
        if (self.relocation_register.get_free_size() < len(program) ):
            raise BSODException()
        else:
            block = self.search_free_block(program)
            if(block is None):
                self.relocation_register.compact() 
                block = self.search_free_block(program)
                
            self.relocation_register.assign_to_block(program, pcb, block) #esto lo tiene que meter en el bloque (agregar su pid y cambiar la longitud)
    
    def search_free_block(self, program):
        blocks = self.relocation_register.get_free_blocks()
        i = 0
        block = blocks[i]
        while(i < len(blocks) and not self.fits(program, block)):
            i += 1
            if i < len(blocks) :
                block = blocks[i]
        if(self.fits(program, block)):
            return block
        else:
            return None

    def fits(self, program, block):
        return block.get_size() >= len(program)
    
    def set_relocation_register(self, reloc):
        self.relocation_register = reloc
    
        
