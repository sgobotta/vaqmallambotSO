'''
Created on 9 de jun. de 2016

@author: Ignacio, santiago
'''
from termcolor import colored

class Loader():

    def __init__(self, memory=None, memory_manager=None, disc=None, output=None):
        self.memory         = memory
        self.memory_manager = memory_manager
        self.disc           = disc
        self.base_address   = 0
        self.free_address   = 0
        
        self.output         = output
        
    def set_base_address(self, memory_address):
        self.base_address = memory_address
    
    def get_instruction(self):
        return self.get(self.base_address)
        
    def get_memory(self):
        return self.memory
    
    def set_memory(self, memory):
        self.memory = memory
        
    def get_free_address(self):
        return self.free_address
         
    def get_memory_manager(self):
        return self.memory_manager
    
    def set_memory_manager(self, memory_manager):
        self.memory_manager = memory_manager
        
    def set_disc(self, disc):
        self.disc = disc
        
    def get_disc(self):
        return self.disc

    def set_output(self, output):
        self.output = output
        
    def load(self, program):
        for instr in program:
            self.memory.put(instr, self.base_address)
            self.log_instruction_loaded(instr)
            self.base_address = self.base_address + 1

    def move_instructions(self, number_of_spaces, start, end):
        instr = start
        while instr < end + 1:
            self.put(self.get(instr), instr - number_of_spaces)
            instr += 1
            
    def put(self, instruction, address):
        self.memory.move(instruction, address)
        
    def get(self, address):
        return self.memory.get(address)
        
    # Disc Calls
    def read(self, program_name):
        return self.disc.file_system[program_name]
    
    def log_instruction_loaded(self, instruction):
        self.memory_manager.log_instruction_loaded(instruction)
        self.output.log(colored("[ LOADER ]", "yellow", attrs=["bold"]) + " >> Instruction '" + instruction + "' loaded in address: [" + str(self.base_address) + "]")
