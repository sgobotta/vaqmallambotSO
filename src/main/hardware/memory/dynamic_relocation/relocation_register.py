'''
El que sabe decir los bloques libres y los ocupados
@author: gabriel y Leonardo
'''
from main.hardware.memory.dynamic_relocation.block import Block
from operator import attrgetter 
from termcolor import cprint
import BSODException

class RelocationRegister:
    
    def __init__(self, memory_manager=None, mem_size=None, memory_assignation_policy=None):
        self.memory_manager = memory_manager
        self.memory_size = mem_size
        self.assignation_policy = memory_assignation_policy
        self.used_map = {}
        self.used = []
        self.free = [Block(0, self.memory_size -1, 0)]
        self.free_size = mem_size
        
        
    def set_mem_size(self, mem_size):
        self.memory_size = mem_size
    
    def strategy_load(self, program, pcb):
        self.alloc(program, pcb)
            
        
    def alloc(self, program, pcb):
        self.assignation_policy.alloc(program, pcb)
        
    def set_memory_assignation_policy(self, assignation_policy):
        self.assignation_policy = assignation_policy
        
    def assign_to_block(self, program, pcb, block):
        # Seteo el PID. Guardo el tamagno anterior. Actualizo el tamagno del bloque con el del programa.
        block.set_pid(pcb.id)
        pcb.set_base_address(block.get_base_address())
        old_size = block.get_size()
        block.set_size(len(program))
        
        # Agregar a used_map
        self.used_map[pcb.pid] = block
        
        # Agrego el bloque modificado a la lista de bloques usados
        self.add_used_block(block)
        # Si el programa no tenia el mismo tamagno que el bloque, creo uno nuevo en la lista de bloques libres
        if(len(program) < old_size):
            start = block.get_base_address() + block.get_size()
            new_block = Block(start, start + old_size - len(program) - 1)
            self.add_free_block(new_block) # tener en cuenta si se agrega debajo de otro libre --> (unificar)
        # Borro el bloque original de la lista de libres
        self.delete_free_block(block)
        #self.merge_free_blocks()
        # Show free table
        self.print_free_blocks()
        self.print_used_blocks()
        #Cargar las instrucciones en memoria.
        self.memory_manager.set_loader_base_address(block.get_base_address())
        self.load(program)
        

    #Se asume que a partir de esa base_adress hasta el limit del program, la memoria puede sobreescribirse
    def load(self, program):
        self.memory_manager.load(program)

    def get_used(self):
        return self.used

    def get_free_blocks(self):
        return self.free

    def add_used_block(self, block):
        self.used.append(block)
        self.used_map[block.get_pid()] = block
        # Actualizar el espacio libre
        self.set_free_size(self.get_free_size() - block.get_size())
        
    def add_free_block(self, block):
        self.free.append(block)

    def set_used(self, value):
        self.used = value


    def set_free(self, value):
        self.free = value


    def get_free_size(self):
        return self.free_size
    
    def set_free_size(self, value):
        self.free_size = value

    def get_base_address(self, pid, pc=None):
        return self.used_map[pid].get_base_address() + pc
        
    def delete_used_block(self, block):
        self.used.remove(block)
        del self.used_map[block.get_pid()]
        # Actualizar el espacio libre
        self.set_free_size(self.get_free_size() + block.get_size())
        
    def delete_free_block(self, block):
        self.free.remove(block)
    
    def compact(self):
            # Mientras haya mas de un bloque libre
            first_free_block = self.search_first_free_block()               # Busco el primer bloque libre
            number_of_spaces = first_free_block.get_size()                  # Saco la cantidad de espacios a mover
            compact_block = self.search_first_used_block(first_free_block)  # Busco el primer bloque usado despues del bloque libre encontrado
            if compact_block is not None:
                self.move_up(compact_block, number_of_spaces)
                self.funcion_magica(compact_block)
                last_block = self.search_last_used_block()
                #self.move_up(compact_block, number_of_spaces)                   # Muevo el bloque usado encontrado x espacios hacia arriba
                self.update_free_block(last_block.get_end() + 1)
            else:
                self.update_free_block(0)
            
    def move_up(self, compact_block, number_of_spaces):
        # Muevo las instrucciones contenidas en el bloque
        self.move_instructions(number_of_spaces, compact_block.get_base_address(), compact_block.get_end())
        # Actualizo el bloque
        compact_block.update(number_of_spaces)
        pcb = self.memory_manager.get_pcb_by_pid(compact_block.get_pid())
        pcb.set_base_address(compact_block.get_base_address())
        
    def update_free_block(self, base_address):
        new_block = Block(base_address, self.memory_size - 1)
        self.set_free([])
        self.free.append(new_block)

        
    def search_first_free_block(self):
    # Retorna el bloque con base_address mas chica, o sea, el que este mas arriba, en definitiva el primero que se encontraria
        block = min(self.free, key=attrgetter('base_address'))
        return block
    
    def search_last_used_block(self):
        block = max(self.used, key=attrgetter('base_address'))
        return block
    
    def search_first_used_block(self, free_block):
        used_block = None
        for block in self.used:
            if self.is_next_used_block(block, free_block):
                used_block = block
        return used_block
                
    def is_next_used_block(self, used_block, free_block):
    # Retorna si el used_block es un bloque contiguo a free_block
        return used_block.get_base_address() == free_block.get_end()+1
        
    def move_instructions(self, number_of_spaces, start, end):
        self.memory_manager.move_instructions(number_of_spaces, start, end)
    

    def merge_free_blocks(self):
        reduce(self.merge_if_are_contiguous, self.free)
        
        
    def merge_if_are_contiguous(self, block1, block2):
        if block1.get_end() +1 == block2.get_base_address():
            new_block = Block(block1.get_base_address(), block2.get_end())
            self.free.remove(block1)
            self.free.remove(block2)
            self.set_free(self.free + [new_block])
            
    def set_memory_manager(self, mem_manager):
        self.memory_manager = mem_manager
        
    def kill_process(self, pcb):
        # borra el bloque con pcb.id() de self.used
        # le borra al bloque el pid
        # lo agrega a la lista de free
        block = self.used_map[pcb.id]
        self.delete_used_block(block)
        block.set_pid(None)
        self.add_free_block(block)
        
    def move_up_used_blocks(self, used_block, number_of_spaces):
        next_used = self.search_first_used_block(used_block)
        if next_used is not None:
            self.move_up(next_used, number_of_spaces)
            contiguous = self.search_next_used_block(next_used)
            if contiguous is not None:
                self.move_up(contiguous, contiguous.get_base_address()-next_used.get_end() )
    
    
    def search_next_used_block(self, used_block):
        otro_block = None
        for block in self.used:
            if self.is_bigger_used_block(block, used_block):
                otro_block = block
        return otro_block
        
    def is_bigger_used_block(self,block, used_block):
        return block.get_base_address() > used_block.get_end()
    
    def funcion_magica(self, compact_block):
        #funcion
        next_used = self.search_next_used_block(compact_block)
    
        if next_used is not None:
            self.move_up(next_used, next_used.get_base_address()-compact_block.get_end() - 1)
            self.funcion_magica(next_used)
        
        
        
    def print_memory(self):
        self.print_free_blocks()
        self.print_used_blocks() 
           
    def print_free_blocks(self):
        cprint("\nFREE BLOCKS:", "green", attrs=["bold"])
        print"_________________________________________________"
        cprint("\nBlock\tSize\tBase Address\t Limit Register", "white", attrs=["bold"])
        print"_________________________________________________"
        i = 0
        for block in self.free:
            print str(i) + "\t" + str(block.get_size()) + "\t" + "\t" + str(block.get_base_address()) + "\t" + "\t" + str(block.get_end())
            i += 1

    def print_used_blocks(self):
        cprint("\nUSED BLOCKS:", "blue", attrs=["bold"])
        print"_________________________________________________"
        cprint("\nPID\tSize\tBase Address\t Limit Register", "white", attrs=["bold"])
        print"_________________________________________________"
        for block in self.used:
            print str(block.get_pid()) + "\t" + str(block.get_size()) + "\t" + "\t" + str(block.get_base_address()) + "\t" + "\t" + str(block.get_end())