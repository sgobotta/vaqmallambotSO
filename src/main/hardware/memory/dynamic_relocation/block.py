'''
Un bloque unico que contiene un solo pcb

@author: gabriel
'''
class Block:
    def __init__(self, a_start_address, limit_register, pid=None):
        self.pid    = pid
        self.base_address  = a_start_address
        self.end    = limit_register
        self.size   = limit_register - a_start_address + 1

    def get_size(self):
        return self.size


    def get_base_address(self):
        return self.base_address


    def get_end(self):
        return self.end


    def get_pid(self):
        return self.pid

    def set_pid(self, pid):
        self.pid = pid

    def set_size(self, value):
        self.size = value
        self.set_limit(self.get_base_address() + self.get_size() -1)


    def set_start(self, value):
        self.base_address = value
        
    def set_base_address(self, base_address):
        self.base_address = base_address

    def set_limit(self, limit):
        self.end = limit
        
    def update(self, number_of_spaces):
        self.base_address   = self.base_address - number_of_spaces
        self.end            = self.end - number_of_spaces
        