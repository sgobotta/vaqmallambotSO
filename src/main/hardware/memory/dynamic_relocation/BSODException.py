'''
Created on 6 de jul. de 2016

@author: gabriel
'''
from termcolor import cprint

class BSODException(Exception):

    def __init__(self):
        
        self.value = "Blue Screen of Death"
        
    def __str__(self):
        print "\n"
        tumba = '''
                                    /MMMMMM/                                    
                                    /MMMMMM/                                    
                                    /MMMMMM/                                    
                                    /MMMMMM/                                    
                           ---------oMMMMMMo---------                           
                           NMMMMMMMMMMMMMMMMMMMMMMMMN                           
                           NMMMMMMMMMMMMMMMMMMMMMMMMN                           
                           NMMMMMMMMMMMMMMMMMMMMMMMMN                           
                           .........oMMMMMMo.........                           
                                    /MMMMMM/                                    
                                    /MMMMMM/                                    
                                    /MMMMMM/                                    
                                `.-:sMMMMMMs:-.`                                
                           `-+sdNNNMMMMMMMMMMNNNds+-`                           
                        `/ymNMMMMMMMMMMMMMMMMMMMMMMNmy/`                        
                      -yNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNs-                      
                    :hNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNh:                    
                  .yNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNy.                  
                 /mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm/                 
                +NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN+                
               +MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM+               
              -NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN-              
              dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd              
             -MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM-             
             oMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMo             
             yMMMMMMMNmmmmmNMMMMMMMMMNmmNMMMMMMNNmmmmmNMMMMMMMMMMMy             
             yMMMMMMN..```..-omMMMMMMo..yMMMMMMo..```.-/dMMMMMMMMMh             
             yMMMMMMm  `ddh+  .NMMMMM+  sMMMMMM/  +dhs. `yMMMMMMMMh             
             yMMMMMMm  `MMMN`  mMMMMM+  sMMMMMM/  yMMMs  /MMMMMMMMh             
             yMMMMMMm  `oo+- .sMMMMMM+  sMMMMMM/  odho. `hMMMMMMMMy             
             yMMMMMMm   --.  /NMMMMMM+  sMMMMMM/  ```.-+dMMMMMMMMMy             
             yMMMMMMm  `MMm/  sMMMMMM+  sMMMMMM/  odmmMMMMMMMMMMMMy             
             yMMMMMMm  .MMMm  `NN+/sM+  sMd//hM/  yMMMMNo/oMMMMMMMh             
             yMMMMMMm``-MMMM+``sd.`-M+``sMs..+M+``yMMMMm.`-NMMMMMMh             
             yMMMMMMMmmmMMMMNmmmMNmNMNmmNMMmmMMNmmNMMMMMNmNMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh             
             +yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy+             
yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
'''
        cprint(tumba, 'white', 'on_blue')
        print "\n"
        #return repr(self.value)
