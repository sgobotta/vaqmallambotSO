
class Disc:
    
    def __init__(self):
        self.file_system = {    'sublime'           : [ 'Iniciando sublime', 'open sublime', 'io_screen', 'end sublime'],
                                'format'            : [ "Iniciando format",'io_cd_burner', 'end format'], 
                                "fifa15.exe"        : [ "Iniciando fifa15", "Cargando Fifa...", "Jugando Fifa...", "io_screen", "Jugando Fifa...", "end fifa15"],
                                "f_Documents"       : ["tp_integrador.odt", "El_secreto_para_aprobar_la_materia.txt", "Modern_Operating_Systems.pdf"],
                                "f_Images"          : ["foto_con_el_yeti.jpg", "Vacaciones_en_Nueva_Dehli.jpg"],
                                "f_Videos"          : ["penales_errados_de_higuain.mkv", "Mr.Robot_s01_e08.mkv"],
                                "f_Music"           : ["Aguantaaaaaa.mp3"],
                                "gta5"              : ["Matar prostitutas", "Robar autos", "Saltar de helicoptero", "Robar banco","Matar prostitutas", "Robar autos", "Saltar de helicoptero", "Robar banco","Matar prostitutas", "Robar autos", "Saltar de helicoptero", "end_gta5"],
                                "notas_musicales"   : ["DO", "RE", "io_cd_burner", "MI", "FA", "io_screen", "SOL", "LA", "SI", "DO", "end escala de Do"] 
                                }
                    