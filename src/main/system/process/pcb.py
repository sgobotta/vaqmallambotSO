'''
Created on 1 de abr. de 2016

@author: sgobotta
'''
from main.system.iocomponents.screen import Screen
from main.system.iocomponents.cd_burner import CDBurner

class Pcb:
    
    def __init__(self, pid, state, priority=None, memory_adress=None, devices_list=None, program_name=None):
        self.pid            = pid
        self.state          = state
        self.priority       = priority
        self.base_address   = memory_adress
        self.devices_list   = [Screen(), CDBurner()]
        self.pc             = 0
        self.program_name   = program_name
      
    def set_program_name(self, program_name):
        self.program_name = program_name
      
    def set_base_address(self, base_address):
        self.base_address = base_address
      
    def set_devices(self, devices_list):
        devices_list = devices_list
        
    def set_state(self,state):
        self.state = state
      
    def get_program_name(self):
        return self.program_name
      
    def state(self):
        return self.state
    
    def priority(self):
        return self.priority
    
    def __str__(self):
        return str(self.id) + "-" + self.state
    
    @property
    def id(self):
        return self.pid
    
    def update_pc(self, value):
        self.pc = value
        
    def get_base_address(self):
        return self.base_address
    
    def print_me(self):
        print str(self.pid) + "\t" + str(self.state) + "\t" + "\t " + str(self.pc) + "\t" + "\t" + str(self.base_address)
