from main.system.process.pcb import Pcb
from main.strings.state import STATE
class PcbTable:
    
    def __init__(self):
        self.last_pid   = 1
        self.pcb_dict   = {}
        self.free_base_address = 0
        
    def create_pcb(self):
        new_pcb = Pcb(self.last_pid, STATE.NEW, 1)
        self.add(new_pcb)
        return new_pcb
        
    def add(self, pcb):
        pcb.id = self.last_pid
        self.pcb_dict[pcb.id] = pcb
        self.increment_last_pid()
    
    def remove(self, pcb):
        pcb_id = pcb.id
        del self.pcb_dict[pcb_id]
    
    def increment_last_pid(self):
        self.last_pid = self.last_pid + 1
        
    def get_last_pid(self):
        return self.last_pid
        
    def get_free_base_address(self):
        return self.free_base_address
        
    def get_processes(self):
        return self.pcb_dict
    
    def get_base_address(self, pid):
        return self.pcb_dict[pid].get_base_address()

    def get_program_name_by_pid(self, pid):
        return self.pcb_dict[pid].program_name

    def is_pcb_loaded(self, pid):
        return pid in self.pcb_dict.keys()
    
    def get_pcb_by_pid(self, pid):
        return self.pcb_dict[pid]