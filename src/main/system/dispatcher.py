from main.strings.state import STATE
from termcolor import colored
class Dispatcher:
    
    def __init__(self, CPU=None, output=None):
        self.cpu        = CPU
        self.pcb        = None
        self.output     = output
    
    def set_cpu(self, cpu):
        self.cpu = cpu
    
    def set_output(self, output):
        self.output = output
    
    def load(self):
        self.cpu.pc             = self.pcb.pc
        self.cpu.pid            = self.pcb.pid
        self.pcb.state          = STATE.RUNNING
                   
    
    def save_pcb(self, pcb):
        self.pcb = pcb
        self.clean_cpu_registers()
        self.load()
        
    def update_pcb(self):
        if self.pcb is not None:
            self.pcb.pc = self.cpu.pc
            self.clean_cpu_registers()


    def clean_cpu_registers(self):
        self.cpu.pc             = None
        self.cpu.pid            = None        

    # Log methods
    def log_queue_empty_status(self):
        self.output.log(colored("[ READY QUEUE ]", "green", attrs=["bold"]) + " >> is empty")
        
        