
class Output:
    
    def __init__(self):
        self.log_list = []
    
    def log(self, string):
        self.log_list.append(string)
        
    def print_log(self):
        for log in self.log_list:
            print(log)