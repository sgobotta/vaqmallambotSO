from main.interruptions.time_out import TimeOut

class Timer:

    def __init__(self, quantum, interruption_manager=None):
        self.quantum                = quantum
        self.current_quantum        = quantum
        self.interruption_manager   = interruption_manager
        
    def set_interruption_manager(self, interruption_manager):
        self.interruption_manager = interruption_manager
        
    def reset(self):
        self.current_quantum = self.quantum
        
    def process(self):
        if self.current_quantum > 0:
            self.current_quantum = self.quantum - 1
        else:
            self.process_time_out_interruption()
            
    def process_time_out_interruption(self):
        self.system.interruption_manager.handle(TimeOut())
