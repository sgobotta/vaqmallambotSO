'''
Created on 12 de abr. de 2016

@author: sgobotta
'''
from main.datastructures.queue import Queue
from main.strings.state import STATE


class Priority:

    def __init__(self, number_of_priorities):

        self.queues = {}
        self.mk_queues(number_of_priorities)

    def mk_queues(self, number_of_priorities):
        for number in range(1, number_of_priorities+1):
            self.queues[number] = Queue()

    def count_queues(self):
        return len(self.queues.keys())

    def priorities(self):
        return self.queues.keys()

    def get_queue_by_priority(self, prio):    
        return self.queues[prio]

    def add(self, pcb):
        pcb_prio = pcb.priority
        pcb.state = STATE.READY
        self.queues[pcb_prio].queue(pcb)

    def get_next(self):
        x = 1
        while x < self.count_queues() and self.queues[x].is_empty():
            x = x + 1
        if not self.queues[x].is_empty():
            return self.queues[x].dequeue()
        
