from main.datastructures.queue import Queue

class Fcfs:
    
    def __init__(self, output=None):
        self.queue = Queue()
        
    def set_output(self, output):
        self.output = output
        
    def add_ready(self, value):
        self.queue.queue(value)
        
    def add(self, value):
        self.add_ready(value)
    
    def get_next(self):
        return self.queue.dequeue()
        
    def get_queue(self):
        return self.queue