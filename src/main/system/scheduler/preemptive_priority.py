'''
Created on May 5, 2016

@author: leonardo
'''
from main.datastructures.queue import Queue
from main.strings.state import STATE

class PreemptivePriority():

    def __init__(self, number_of_priorities, kernel=None):
        
        self.kernel = kernel
        self.queues = {}
        self.mk_queues(number_of_priorities)
        
    def set_kernel(self, kernel):
        self.kernel = kernel 
       
    def mk_queues(self, number_of_priorities):
        for number in range(1, number_of_priorities+1):
            self.queues[number] = Queue()
            
    def count_queues(self):
        return len(self.queues.keys())

    def priorities(self):
        return self.queues.keys()

    def get_queue_by_priority(self, prio):    
        return self.queues[prio]

    def add(self, pcb):
        
        pcb_running = self.kernel.get_running_process()
        
        if pcb.priority < pcb_running.priority:
            pcb_running.state = STATE.READY
            self.queues[pcb_running.priority].queue(pcb_running)
            self.kernel.dispatcher_load(pcb)
            
        else:
            pcb.state = STATE.READY
            self.queues[pcb.priority].queue(pcb)

    def get_next(self):
        x = 1
        while x < self.count_queues() and self.queues[x].is_empty():
            x = x + 1
        if self.queues[x] is not None:
            return self.queues[x].dequeue()