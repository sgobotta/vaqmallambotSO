'''
Created on May 17, 2016

@author: santiago
'''
from main.system.scheduler.fcfs import Fcfs
from main.datastructures.queue import Queue
from termcolor import colored

class Screen:
    
    def __init__(self, output=None, interruption_manager=None):
        self.queue                  = Queue()
        self.output                 = output
        self.name                   = "screen"
        self.running                = None
        self.interruption_manager   = interruption_manager
        
    def set_interruption_manager(self, interruption_manager):
        self.interruption_manager = interruption_manager
        
    def set_output(self, output):
        self.output = output
        
    def add_process(self, process):
        self.queue.queue(process)
        
    def remove_process(self):
        pcb = self.running
        self.running = None
        return pcb
        
    def process(self):
        current = self.queue.dequeue()
        self.running = current
        if current is not None:
            self.log_process_id_print(current)
            self.interruption_manager.handle("out_io_screen")
            
    def get_running(self):
        return self.running
            

    
    # Log methods
    
    def log_process_id_print(self, pcb):
        self.output.log(colored("[ DEVICE: SCREEN ]", "white", attrs=["bold"]) + " >> Printing process with id: " + str(pcb.pid) + " ]")