'''
Created on May 20, 2016

@author: santiago
'''
from main.datastructures.queue import Queue
from termcolor import colored

class CDBurner:
    
    def __init__(self, output=None, interruption_manager=None):
        self.queue                  = Queue()
        self.output                 = output
        self.name                   = "cd_burner"
        self.running                = None
        self.interruption_manager   = interruption_manager
        
    def set_interruption_manager(self, interruption_manager):
        self.interruption_manager = interruption_manager
        
    def set_output(self, output):
        self.output = output
        
    def add_process(self, process):
        self.queue.queue(process)
        
    def remove_process(self):
        pcb = self.running
        self.running = None
        return pcb
        
    def process(self):
        current = self.queue.dequeue()
        self.running = current
        if current is not None:
            self.log_process_id_print(current)
            print(self.interruption_manager)
            self.interruption_manager.handle("out_io_cd_burner")
            
    def get_running(self):
        return self.running
            

    
    # Log methods

    def log_process_id_print(self, pcb):
        self.output.log(colored("[ DEVICE: CD-BURNER ]", "white", attrs=["bold"]) + " >> Printing process with id: " + str(pcb.pid) + " ]")