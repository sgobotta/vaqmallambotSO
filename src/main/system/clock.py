from datetime import datetime
from termcolor import colored
class Clock:
    
    def __init__(self, output=None):
        self.listeners = []
        self.output = output
        
    def set_output(self, output):
        self.output = output
        
    def add_listener(self, listener):
        self.listeners.append(listener)
        
    def ticks(self):
        self.log_tick_time()
        for listener in self.listeners:
            listener.process()
            
    def log_tick_time(self):
        self.output.log(colored("__________________________________________________________________________\n", attrs=["bold"]))
        self.output.log(colored("[ CLOCK ]", "blue", attrs=["bold"]) + " >> Tick at " + str(datetime.now()))
            
    def __getattr__(self, attr):
        print attr
        return None
    