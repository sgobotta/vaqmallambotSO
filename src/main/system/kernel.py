from main.system.process.pcb_table import PcbTable
from main.system.process.pcb import Pcb
from main.strings.state import STATE
from main.system.iocomponents.screen import Screen
from main.system.iocomponents.cd_burner import CDBurner

class Kernel:
    
    def __init__(self, dispatcher=None, scheduler=None, disc=None, screen_device=None, memory_manager=None, devices_list=None, loader=None):
        
        self.running_process = None
        
        self.pcb_table  = PcbTable()
        
        self.dispatcher = dispatcher
        self.scheduler  = scheduler
        
        self.loader     = loader
        
        self.screen_device  = screen_device
        self.memory_manager = memory_manager
        
        self.devices_list   = [Screen(), CDBurner()]
        
    # SETTERS
    
    def set_pcb_table(self, pcb_table):
        self.pcb_table = pcb_table
        
    def set_dispatcher(self, dispatcher):
        self.dispatcher = dispatcher
        
    def set_scheduler(self, scheduler):
        self.scheduler = scheduler
        
    def set_screen_device(self, screen_device):
        self.screen_device = screen_device

    def set_memory_manager(self, memory_manager):
        self.memory_manager = memory_manager
    
    def set_devices_list(self, devices_list):
        self.devices_list = devices_list
        
    def set_loader(self, loader):
        self.loader = loader
    
    def set_running_process(self, pcb):
        if self.running_process is None:
            self.running_process = pcb
        
    
    def do_context_switching(self, pcb):
        self.update_dispatcher()
        pcb     = self.get_pcb_from_dispatcher()
        new_pcb = self.get_next()
        self.save_on_dispatcher(new_pcb)
        self.add_to_scheduler(pcb)

    def get_running_process(self):
        return self.running_process

    def add_to_pcb_table(self, pcb):
        self.pcb_table.add(pcb)
    
    # Cuando aislamos el system de io_end, en vez de llamar al scheduler, lo  hacemos a traves del kernel
    
    # SCHEDULLER CALLS
    def get_next_from_scheduler(self):
        return self.scheduler.get_next()
        
    def add_to_scheduler(self, pcb):
        self.scheduler.add(pcb)
    
    def get_next_from_device_queue(self, device):
        return device.remove_process()
    
    # DEVICE QUEUES
    def add_to_device_queue(self, pcb, device):
        self.running_process = None
        device.add_process(pcb)
        
        # DEVICES GETTER
    def get_device_by_name(self, device_name):
        print device_name
        new_device = None 
        for device in self.devices_list:
            if device.name == device_name:
                new_device = device
        return new_device
        
    # Loader CALLS
    def read(self, program_name):
        return self.loader.read(program_name)
        
    # MEMORY CALLS
    def get_free_address(self):
        return self.memory_manager.get_free_address() #SE DEBE CONSULTAR A UN MEMORY MANAGER
        
    def load(self, program):
        self.loader.load(program)
        
    def load_in_memory(self, program, pcb):
        self.memory_manager.strategy_load(program, pcb)
        
    # PCB_TABLE CALLS
    def get_last_pid(self):
        return self.pcb_table.last_pid
    
    def create_pcb(self):
        return self.pcb_table.create_pcb()
    
    def remove_from_pcb_table(self, pcb):
        self.pcb_table.remove(pcb)
        self.memory_manager.kill_process(pcb)
        self.running_process = None    
    
    # DISPATCHER CALLS
    def dispatcher_load(self, pcb):
        self.dispatcher.save_pcb(pcb)
        
    def get_base_address(self, pid):
        return self.pcb_table.get_base_address(pid)

    def get_program_name_by_pid(self, pid):
        return self.pcb_table.get_program_name_by_pid(pid)
    
    def is_pcb_loaded(self, pid):
        return self.pcb_table.is_pcb_loaded(pid)
    
    def get_pcb_by_pid(self, pid):
        return self.pcb_table.get_pcb_by_pid(pid)