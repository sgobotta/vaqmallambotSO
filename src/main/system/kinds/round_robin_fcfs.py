from main.system.log.output import Output
from main.system.kernel import Kernel
from main.system.clock import Clock
from main.hardware.memory.memory import Memory
from main.hardware.disc import Disc
from main.system.scheduler.non_preemtive_priority import Priority
from main.hardware.cpu import CPU
from main.system.dispatcher import Dispatcher
from main.interruptions.interruption_manager import InterruptionManager
from main.system.process.pcb_table import PcbTable
from main.system.iocomponents.screen import Screen
from main.hardware.memory.memory_manager import MemoryManager
from main.system.kinds.shell import Shell
from main.system.iocomponents.cd_burner import CDBurner
from main.hardware.memory.loader import Loader
from main.interruptions.handler import Handler
from main.system.timer import Timer



class PrioritySystem:
    
    def __init__(self, priority):
        
        # ***********************************************************
        # ***************************** Components Creation *********
        # ***********************************************************
        
        self.shell                  = Shell()
        
        self.output                 = Output()
        
        self.kernel                 = Kernel()
        
        self.memory_manager         = MemoryManager()
        
        self.loader                 = Loader()
        
        self.clock                  = Clock()
        
        self.memory                 = Memory()
        self.disc                   = Disc()
        
        self.scheduler              = Priority(priority)
        
        self.timer                  = Timer(3)
        
        self.cpu                    = CPU()
        
        self.dispatcher             = Dispatcher()
        
        self.handler                = Handler()
        
        self.interruption_manager   = InterruptionManager(self)
        
        self.pcb_table              = PcbTable()
        
        self.devices                = []
        self.screen_device          = Screen()
        self.cd_burner              = CDBurner()
        
        self.devices.append(self.screen_device)
        self.devices.append(self.cd_burner)

        # ***********************************************************
        # ***************************** Setting components **********
        # ***********************************************************

        # Shell
        self.shell.set_interruption_manager(self.interruption_manager)
    
        # Kernel
        self.kernel.set_dispatcher(self.dispatcher)
        self.kernel.set_scheduler(self.scheduler)
        self.kernel.set_pcb_table(self.pcb_table)
        self.kernel.set_loader(self.loader)
        self.kernel.set_screen_device(self.screen_device)
        self.kernel.set_memory_manager(self.memory_manager)
        self.kernel.set_devices_list(self.devices)
    
        # Memory Manager
        self.memory_manager.set_output(self.output)
        self.memory_manager.set_loader(self.loader)
        
        # Loader
        self.loader.set_memory(self.memory)
        self.loader.set_memory_manager(self.memory_manager)
        self.loader.set_disc(self.disc)
        self.loader.set_output(self.output)
    
        # CPU
        self.cpu.set_interruption_manager(self.interruption_manager)
        self.cpu.set_memory(self.memory)
        self.cpu.set_output(self.output)
    
        # Dispatcher
        self.dispatcher.set_cpu(self.cpu)
        self.dispatcher.set_output(self.output)
        
        # Interruption Manager
        self.interruption_manager.set_kernel(self.kernel)
        self.interruption_manager.set_dispatcher(self.dispatcher) #se puede cambiar por self
        self.interruption_manager.set_output(self.output)
        self.interruption_manager.set_handler(self.handler)
        
        # Devices
        self.screen_device.set_output(self.output)
        self.cd_burner.set_output(self.output)
        
        # Clock
        self.clock.set_output(self.output)
        self.clock.add_listener(self.interruption_manager)
        self.clock.add_listener(self.cpu)
        self.clock.add_listener(self.screen_device)
        self.clock.add_listener(self.cd_burner)
    
        #Timer
        self.timer.set_interruption_manager(self.interruption_manager)