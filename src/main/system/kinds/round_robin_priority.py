from Output import *
from Memory import *
from harddrive import *
from Scheduler import Priority
from CPU import *
from Dispatcher import *
from Interruptions import InterruptionManager
from Kernel import *
from Timer import *


class RoundRobinPrioritySystem:
    
    def __init__(self, quantums):
    
        self.output = Output()
        
        self.memory                 = Memory(self)
        self.disc                   = Disc(self)
        self.screen                 = None
        self.scheduler              = Priority()
        self.cpu                    = CPU(self)
        self.dispatcher             = Dispatcher(self)
        self.interruption_manager   = InterruptionManager(self)
        self.kernel                 = Kernel(self)
        self.timer                  = Timer(quantums, self)
        