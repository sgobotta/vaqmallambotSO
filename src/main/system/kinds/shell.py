class Shell:
    
    def __init__(self, interruption_manager=None):
        self.interruption_manager = interruption_manager
        
    def set_interruption_manager(self, interruption_manager):
        self.interruption_manager = interruption_manager
        
    def input(self, string):
        self.interruption_manager.handle(string)