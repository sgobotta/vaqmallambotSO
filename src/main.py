'''
Created on 15 de may. de 2016

@author: Gabriel
'''

from main.system.kinds.priority import PrioritySystem
from main.system.scheduler.fcfs import Fcfs
from main.system.scheduler.non_preemtive_priority import Priority
from main.system.scheduler.preemptive_priority import PreemptivePriority
from termcolor import colored, cprint
import os
import sys
import time
from main.hardware.memory.memory_assignation_policy.first_fit import FirstFit
from main.hardware.memory.memory_assignation_policy.worst_fit import WorstFit
from main.hardware.memory.memory_assignation_policy.best_fit import BestFit
from main.hardware.memory.dynamic_relocation.relocation_register import RelocationRegister
from main.hardware.memory.static_memory.static_memory import StaticMemory
from main.hardware.memory.paging.algorithms.FIFO import Fifo
from main.hardware.memory.paging.algorithms.LRU import LRU
from main.hardware.memory.paging.algorithms.second_chance import SecondChance
from main.hardware.memory.paging.paging_strategy import Paging
from main.interruptions.paging_handler import PagingHandler


class Main:
    
    def __init__(self):
        
        self.system = PrioritySystem(3)
        
        
m = Main()

logo = '''                                                   ``                                     
                                                                                                               
                                                                                                               
                                `                                                                              
 @    @    ;@     #@@@+   @@   @#    @+    @     @       :@    @@,  @@  @@@@   `@@@@   @@@@@@      #@@@'  `@@@@
 @@  +@    @@;   ,@   @.  @@: @@@   ,@@    @     @       @#'   @@@ `@@  @  #,  @:  @@    @.       ;@   @` @+   
 ,@  @'    @`@   @+   #@  @'@ @'@   @,@`  `@     @       @`@   #@@`@#@  @  @` .@    @    @,       @'   @@ @@   
  @;,@    @# @   #:   ;@  @`@@'@@   @ '@  `@     @      #@ @`  #@:@@ @  @@@@` ;@    @    @:       @,   '@` @@@#
  @@@@    @::@#  @# ` @@  @.@@.@@  +@::@  `@     @      @;:@@  @@:@@ @  @  .@ .@   `@    @;       @+   @@     @
  ,@@;   :@@@@@  ,@@@@@,  @,'@ @@  @@@@@' `@###  @###, ,@@@@@  @@ @'`@  @'+@@  @@@@@@    @;       ;@@@@@` @' :@
   @@    @@   @:  #@@@@   @,   @@ .@   @@ `@@@@  @@@@. @@   @; @@   `@  @@@@:  .@@@@     @'        @@@@+  @@@@@
                     @@+                                                                                       
''' 


def show_logo():
    for char in logo:
        time.sleep(0.001)
        sys.stdout.write(colored(char, 'red'))
        sys.stdout.flush()

def process_loop():
    for num in range(1, 10):
        m.system.clock.ticks()
        m.system.output.print_log()
        
def tick_20():
    for i in range(1, 20):
        m.system.clock.ticks()
        m.system.output.print_log()
 
def ps():
    procesos = m.system.pcb_table.get_processes()
    print ""
    print colored("PCB TABLE", attrs=["bold"])
    print "__________________________________________________________________________\n"
    print colored("PID \t STATE \t \t PC \t Base Address", "white", attrs=["bold"])
    print "__________________________________________________________________________"
    for key in procesos.keys():
        procesos[key].print_me()
    print ""
        
def print_memory_blocks():
    m.system.memory_manager.allocation_strategy.print_free_blocks()
    m.system.memory_manager.allocation_strategy.print_used_blocks()
    ps()

def printPagingTable():
    paging_table = m.system.memory_manager.allocation_strategy.paging_table_manager.paging_table
    print "\t"
    print colored("PAGING TABLE", attrs=["bold"])     
    print "__________________________________________________________________________\n"
    print colored("PID \t PAGE \t FRAME \t LOAD \t SWAP \t USAGE", "white", attrs=["bold"])
    print " " 
    
    pids   = paging_table.get_pid_list()
    pages  = paging_table.get_page_list()
    frames = paging_table.get_frame_list()
    load   = paging_table.get_load_list()
    swap   = paging_table.get_swap_list()
    usage  = paging_table.get_usage_list()
    x = 0
    for i in paging_table.get_pid_list():
        print str(pids[x]) + " \t " + str(pages[x]) + " \t " + str(frames[x]) + " \t " + str(load[x]) + " \t" + str(swap[x]) + " \t " + str(usage[x])
        x = x + 1
    print " "

def tick():
    m.system.clock.ticks()
    m.system.output.print_log()
    
def detailedTick():
    m.system.clock.ticks()
    m.system.output.print_log()
    m.system.memory_manager.allocation_strategy.print_memory()
    ps()
    
def sublime():
    m.system.shell.input("./sublime")
    m.system.clock.ticks()
    ps()

def format():
    m.system.shell.input("./format")
    m.system.clock.ticks()
    ps()

def print_file_by_type(file):
    if str(file).startswith("f_"):
        folder = file[2:].strip()
        print colored(folder, "cyan", attrs=["bold"])
    else:
        print file 
            
def ls():
    files = m.system.disc.file_system

    for file in files.keys():
        print_file_by_type(file)


def cd(directory):
    files = m.system.disc.file_system
    contents = files["f_" + directory]
    for f in contents:
        print_file_by_type(f)

def fifa15():
    m.system.shell.input("./fifa15.exe")
    m.system.clock.ticks()
    ps()
    
def help():
    instrucciones1 = "Programs: " + "\n" + "\t" + "[GTA V]" + "\n" + "\t" + "[fifa15]" + "\n" + "\t" + "[format]" + "\n" + "\t" + "[sublime]" + "\n" + "\t" + "[notas_musicales]" + "\n" + "Commands:" + "\n" + "\t" + "[blocks - Shows the logical memory.]" + "\n" + "\t" + "[cd - Change the current directory.]" + "\n" + "\t" + "[dtick - Detailed tick.]" + "\n" + "\t" + "[exit - Cause normal process termination.]" + "\n" + "\t" + "[help - Displays information about internal orders.]" + "\n" + "\t" + "[ls - List directory contents.]" + "\n" + "\t" + "[ps - Report a snapshot of the current processes.]" + "\n" + "\t" + "[ptable - Print Paging Table.]" + "\n" + "\t" + "[tick - Runs a system burst.]" + "\n" + "\t" + "[tick10 - Runs ten system bursts.]" + "\n" + "\t" + "[tick20 - Runs twenty system bursts.]"           
    print instrucciones1

def pMem():
    print colored("RAM: ", attrs=['bold'])+ "\n" + str(m.system.memory.ram)

def notas_musicales():
    m.system.shell.input("./notas_musicales")
    m.system.clock.ticks()
    ps()

def gta_v():
    m.system.shell.input("./gta5")
    m.system.clock.ticks()
    ps()  

def process_input(input_algo):
    options = {
         'tick'     : tick,
         'ps'       : ps,
         'tick10'   : process_loop,
         'tick20'   : tick_20,
         'exit'     : exit,
         'fifa15'   : fifa15,
         'sublime'  : sublime,
         'format'   : format,
         'ls'       : ls,
         'help'     : help,
         'ptable'   : printPagingTable,
         'dtick'    : detailedTick,
         'pmem'     : pMem,
         'blocks'   : print_memory_blocks,
         'gta5'     : gta_v,
         'notas_musicales' : notas_musicales
        }
    if input_algo.startswith("cd ") and (m.system.disc.file_system.has_key("f_" + input_algo[3:].strip())):
        cd(input_algo[3:].strip())
    elif(options.has_key(input_algo)):
            options[input_algo]()
    else:
        if input_algo.startswith("cd "):
            print "Invalid directory!"
            ls()
        else:
            print "Invalid command!"
            help()
            
def configure_preemptive_priority():
    scheduler = PreemptivePriority(3)
    m.system.kernel.set_scheduler(scheduler)
    m.system.scheduler = scheduler
    m.system.scheduler.set_kernel(m.system.kernel)

def configure_scheduler():
    print("\nSeleccione el tipo de Scheduler:")
    print("\n\t1. FCFS\n\t2. NON-PREEMPTIVE PRIORITY\n\t3. PREEMPTIVE PRIORITY")
    
    option1 = raw_input(colored("\n\nroot@vaqmallambotOS $ ", "green", attrs=["bold"] ))
    schedulers = {"1": Fcfs,
                  "2": configure_priority,
                  "3": configure_preemptive_priority}
    while(not schedulers.has_key(option1)):
        print "Invalid option!, try again"
        option1 = raw_input(colored("\n\nroot@vaqmallambotOS $ ", "green", attrs=["bold"] ))
    
    schedulers.get(option1)()
    #m.system.kernel.set_scheduler(scheduler)
        

def configure_priority():
    m.system.kernel.set_scheduler(Priority(3))
    
def configure_relocation_register():
    mem_manager = m.system.memory_manager
    reloc = RelocationRegister(mem_manager, 32)
    m.system.memory_manager.set_allocation_strategy(reloc)
    configure_policy()

def configure_policy():
    print("\nSeleccione la Politica de Asignacion de Memoria:")
    print("\n\t1. FirstFit\n\t2. BestFit\n\t3. WorstFit")
    
    option3 = raw_input(colored("\n\nroot@vaqmallambotOS $ ", "green", attrs=["bold"] ))
    politicas = {"1": FirstFit,
                 "2": BestFit,
                 "3": WorstFit}
    while(not politicas.has_key(option3)):
        print "Invalid option!, try again"
        option3 = raw_input(colored("\n\nroot@vaqmallambotOS $ ", "green", attrs=["bold"] ))
        
    politica = politicas.get(option3)()
    m.system.memory_manager.allocation_strategy.set_memory_assignation_policy(politica)
    politica.set_relocation_register(m.system.memory_manager.allocation_strategy)
      
    
def configure_paging():
    m.system.memory_manager.set_allocation_strategy(Paging(3, 3)) #frame, memory sizes
    m.system.memory_manager.allocation_strategy.set_loader(m.system.loader)
    m.system.memory_manager.allocation_strategy.set_interruption_manager(m.system.interruption_manager)
    m.system.memory_manager.allocation_strategy.set_output(m.system.output)
    m.system.interruption_manager.set_handler(PagingHandler())
    m.system.memory_manager.allocation_strategy.swap_manager.set_interruption_manager(m.system.interruption_manager)
    m.system.memory_manager.allocation_strategy.swap_manager.set_loader(m.system.loader)
    m.system.memory_manager.allocation_strategy.swap_manager.set_output(m.system.output)
    configure_paging_policy()

def configure_paging_policy():
    print("\nSeleccione el algoritmo de paginacion:")
    print("\n\t1. FIFO\n\t2. LRU\n\t3. SecondChance")
    option4 = raw_input(colored("\n\nroot@vaqmallambotOS $ ", "green", attrs=["bold"] ))
    algorithms = {"1": Fifo,
                 "2": LRU,
                 "3": SecondChance}
    while(not algorithms.has_key(option4)):
        print "Invalid option!, try again"
        option4 = raw_input(colored("\n\nroot@vaqmallambotOS $ ", "green", attrs=["bold"] ))
        
    algorithm = algorithms.get(option4)()
    m.system.memory_manager.allocation_strategy.set_policy(algorithm)
    m.system.memory_manager.allocation_strategy.policy.set_table_manager(m.system.memory_manager.allocation_strategy.paging_table_manager)
    m.system.memory_manager.allocation_strategy.policy.set_swap_manager(m.system.memory_manager.allocation_strategy.swap_manager)
    m.system.memory_manager.allocation_strategy.policy.set_output(m.system.output)

def configure_static_memory():
    memory = StaticMemory()
    memory.set_memory_manager(m.system.memory_manager)
    memory.set_kernel(m.system.kernel)
    m.system.memory_manager.set_allocation_strategy(memory)
    
def configure_allocation_strategy():
    print("\nSeleccione la Estrategia de Alocacion de Memoria:")
    print("\n\t1. Memoria Estatica\n\t2. Asignacion Continua\n\t3. Paginacion")
    option2 = raw_input(colored("\n\nroot@vaqmallambotOS $ ", "green", attrs=["bold"] ))
    politicas = {"1": configure_static_memory,
                 "2": configure_relocation_register,
                 "3": configure_paging}
    while(not politicas.has_key(option2)):
        print "Invalid option!, try again"
        option2 = raw_input(colored("\n\nroot@vaqmallambotOS $ ", "green", attrs=["bold"] ))
    
    politicas.get(option2)()
    
    
    

# ==========================================
# Se ejecuta el programa
# ==========================================

# Clear Screen inicial
os.system('cls' if os.name == 'nt' else 'clear')
# Mostrar el logo
show_logo()

#===============================================================================
# Configuro el sistema
#===============================================================================
cprint("\n\nConfiguracion de Inicio", "white", attrs=["bold"])
# Scheduling
configure_scheduler()
# Allocation Strategy
configure_allocation_strategy()

#===============================================================================
# Proceso la entrada del usuario
#===============================================================================
print("\nEnter a command...")
while (True):
    input1 = raw_input(colored("\n\nroot@vaqmallambotOS $ ", "green", attrs=["bold"] ))
    os.system('cls' if os.name == 'nt' else 'clear')
    process_input(input1)
    
# listar directorios en disco (ls)
# listar procesos ejecutandose (ps)
# mem_status (memoria libre)
# configuracion dinamica
# Threads
