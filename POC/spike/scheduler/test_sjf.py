'''
Created on 28 de abr. de 2016

@author: ignaciol
'''
import unittest
from Scheduler.Sjf import Sjf
from Process.Pcb import *
from strings.state import STATE


class TestSjf(unittest.TestCase):

    def setUp(self):
        
        self.pcb1 = Pcb(STATE.READY, "pid01", 0, 0, 15)
        self.pcb2 = Pcb(STATE.READY, "pid02", 0, 0, 12)
        self.pcb3 = Pcb(STATE.READY, "pid03", 0, 0, 1)
        self.pcb4 = Pcb(STATE.READY, "pid04", 0, 0, 35)
        self.pcb5 = Pcb(STATE.READY, "pid05", 0, 0, 15)
        self.pcb6 = Pcb(STATE.READY, "pid06", 0, 0, 25)
        
        self.my_sjf = Sjf()
    
    def test_add_sjf(self):
        
        self.my_sjf.add(self.pcb1)
        self.my_sjf.add(self.pcb2)
        self.my_sjf.add(self.pcb3)
        self.my_sjf.add(self.pcb4)
        self.my_sjf.add(self.pcb5)
        self.my_sjf.add(self.pcb6)

        next_pcb = self.my_sjf.get_next()
        self.assertEquals(next_pcb, self.pcb3)
        
        next_pcb = self.my_sjf.get_next()
        self.assertEquals(next_pcb, self.pcb2)
        
        next_pcb = self.my_sjf.get_next()
        self.assertEquals(next_pcb, self.pcb5)

        next_pcb = self.my_sjf.get_next()
        self.assertEquals(next_pcb, self.pcb1)

        next_pcb = self.my_sjf.get_next()
        self.assertEquals(next_pcb, self.pcb6)
        
        next_pcb = self.my_sjf.get_next()
        self.assertEquals(next_pcb, self.pcb4)   
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testSjf']
    unittest.main()