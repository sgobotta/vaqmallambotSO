'''
Created on 28 de abr. de 2016

@author: ignaciol
'''
from Queues.Queue import Queue

class Sjf:
    
    def __init__(self):
        self.queue = Queue()
        
    def add(self, pcb):
        pcb_estimated_time = pcb.estimated_time()
        new_queue = Queue()
        while not self.queue.is_empty() and pcb_estimated_time > self.queue.first().estimated_time():
            new_queue.queue(self.queue.first())
            self.queue.dequeue()
        new_queue.queue(pcb)
        while not self.queue.is_empty():
            new_queue.queue(self.queue.first())
            self.queue.dequeue()
        self.queue = new_queue
        
    def get_next(self):
        return self.queue.dequeue()