from Tree import *

class EmptyT(Tree):
    
    def __init__(self):
        self.root           = None
        self.leaf_child     = None
        self.right_child    = None
        
    
    def sizeT(self):
        return 0
    
    
    def belongsT(self, value):
        return False