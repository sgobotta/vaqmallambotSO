from Tree import *

class NodeT(Tree):
    
    def __init__(self, root, l_child, r_child): # NodeT( 1, EmptyT(), EmptyT() )
        self.root           = root
        self.left_child     = l_child
        self.right_child    = r_child      
 
    
    def root(self):
        return self.root
    
    
    def left_child(self):
        return self.left_child
    
    
    def right_child(self):
        return self.right_child