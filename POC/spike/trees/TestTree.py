from Trees.NodeT import *
from Trees.EmptyT import *

import unittest

class TestTree(unittest.TestCase):
    
    def setUp(self):
        self.empty_tree  = EmptyT()
        self.new_tree    = NodeT(1, EmptyT(), EmptyT())
        
        
    def test_sizeT_emptyT_and_NodeT_whit_one_root(self):
        
        self.assertEquals(self.empty_tree.sizeT, 0)
        self.assertEquals(self.new_tree.sizeT, 1)
        
        
if __name__ == '__main__':

  unittest.main()