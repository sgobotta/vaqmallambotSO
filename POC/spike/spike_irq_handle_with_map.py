from main.interruptions.kill import Kill
from POC.spike.spike_irq_handle_with_map import Interruptoin_manager

class Interruption_manager:

    def __init__(self):
        self.int_request = None
        self.methods_map = {
                            "end": kill, 
                            "io": ioIn,
                            "./": new,
                            "out_io": ioEnd,
                            "time_out": timeOut
                            }

    def evaluate_irq(self, irq):
        map_list = self.methods_map.iterkeys()
        
        for k in map_list:
            if(irq.lower().startswith(k)):
                self.methods_map[k](irq,self)

        return self.int_request

    def kill(irq,self):
        self.int_request = Kill(irq,self)

    def ioIn(irq,self):
        self.int_request = ioIn(irq,self)

    def new(program_name, self):
        self.int_request = New(program_name, self)

    def ioEnd(irq,self):
        self.int_request = IOEnd(irq,self)

    def timeOut(irq,self):
        self.int_request = TimeOut(irq, self)
        


   
        
def test_1(self):
    print "entre aca"
    interruption = Interruption_manager()
    interruption.evaluate_irq("end")  
    
    self.assertEquals(interruption.int_request, Kill(irq,interruption))        