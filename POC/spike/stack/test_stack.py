'''
Created on 11 de abr. de 2016

@author: ignacio
'''
from Stack.Stack import Stack

import unittest

class TestStack(unittest.TestCase):

    def setUp(self):
        self.my_stack = Stack()
        self.my_stack.push(8)
        self.my_stack.push(7)
        self.my_stack.push(6)
        self.my_stack.push(5)
        self.my_stack.push(4)
        self.my_stack.push(3)
        self.my_stack.push(2)
        self.my_stack.push(1)

    def test_push_element(self):
        self.assertEquals(self.my_stack.top.value, 1)
        self.assertEquals(self.my_stack.top.next.value, 2)
        self.assertEquals(self.my_stack.top.next.next.value, 3)
        
    """    
        self.assertTrue(self.my_stack.contains(1))
        self.assertTrue(self.my_stack.contains(2))
        self.assertTrue(self.my_stack.contains(3))
        self.assertTrue(self.my_stack.contains(4))
     """ 
    def test_pop_element(self):
          
        self.assertEquals(self.my_stack.pop(), 1)
        
        self.my_stack.pop()      
        self.assertEquals(self.my_stack.top.value, 3)
    """            
        self.assertFalse(self.my_stack.contains(1))
        
        self.assertTrue(self.my_stack.contains(2))
        
        self.assertEquals(self.my_stack.length, 7)
    """   
    def test_remove_all_elements(self):
        
        self.my_stack.remove(1)
        self.my_stack.remove(2)
        self.my_stack.remove(3)
        self.my_stack.remove(4)
        self.my_stack.remove(5)
        self.my_stack.remove(6)
        self.my_stack.remove(7)
        self.my_stack.remove(8)
        
        self.assertEquals(self.my_stack.top, None)
    
    """      
    def test_get_element(self):
      
        self.assertEquals(self.my_stack.get(1), 1)
        self.assertEquals(self.my_stack.get(2), 2)
        self.assertEquals(self.my_stack.get(3), 3)
    """        
    def test_length(self):
      
        self.assertEquals(self.my_stack.length, 8)
        
    
if __name__ == '__main__':

        unittest.main()