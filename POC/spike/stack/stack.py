'''
Created on 11 de abr. de 2016

@author: ignacio
'''
from Lists.Node import *

class Stack:

    def __init__(self):
        self.top = None
        self.length = 0

    def top(self):
        return self.top
    
    def pop(self):
        top = self.top.value
        self.remove(self.top.value)
        return top
    
    def is_empty(self):
        return self.top == None
    
    # Adds a new node to self
    def push(self, value):
        node = Node(value)
        if self.top is None:
            self.top = node
        else:
            node.set_next(self.top)
            self.top = node
        self.length += 1
    def remove(self, value):
        current = self.top
        # No estoy en una lista vacia y no encontre el elemento que buscaba.
        while current is not None and current.value is not value:
            current = current.next
        # Si sali del while y el current no es vacio, entonces encontre al elemento que buscaba
        if current is not None:
            # si no es el primer elemento de la lista
            if current.previous is not None:
                current.previous.set_next(current.next)
            else:
                self.top = current.next
            # si no es el ultimo de la lista
            if current.next is not None:
                current.next.set_previous(current.previous)
            
            del current
            self.length -= 1        
    
    """        
    # Returns the first element with value "value"
    def get(self, value):
        current = self.first
        target = None
        if current is not None:
            while current.value is not value and current is not None:
                current = current.next
            if current.value is value:
                target = current.value
            else:
                #Throw Exception
                pass
        return target

    # Search for an element with value "value" to return a Boolean
    def contains(self, value):
        current = self.first
        while current is not None and current.value is not value:
            current = current.next
        return current is not None
    """
    # Returns the number of elements contained
    def length(self):
        return self.length
    
    def printStack(self):
        current = self.first
        print ("[ ")
        while current is not None:
            if current.next is not None:
                print(current.value)
                print ", "
            else: 
                print (current.value)
                print ("]")
            current = current.next
