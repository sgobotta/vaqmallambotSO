# mensajes para la memoria: put(intruction, memory_address), agrega una instruccion en una direccion de memoria.
#							get(memory_address), pedir u obtener una instruccion de una direccion de memoria.
#
# El dispatch va a inicializar los registros del CPU:
#													  * PC: empieza en 0
#													  * BaseDir: la direccion donde esta la primera instruccion (ej. AAAA0)
#													  * Intruction: cuando fetchea guada la instruccion que tiene que ejecutar
#													                cuando fetchea incrementa el pc para tener la siguiente 
#																	instruccion y se produce un TIC del Clock.
#
# PCB Structure:
#				PID (program ID): x
#				Priority: x
#				PC: 0
#				BaseDir (direccion de memoria donde comienza el programa): AAA0

class Memory:
	def __init__(self):
		self.ram = [] 

	def put(self, instruction, memory_address):
		self.ram[memory_address] = instruction

	def get(self, memory_address):
		return self.ram[memory_address]



class Cpu:
	def __init__(self, pcb, memory, scheduler):
		self.pc 			= pcb.pc
		self.base_address 	= pcb.base_address
		self.instruction	= None
		self.memory 		= memory
		self.scheduler		= scheduler

	def fetch_intruction(self):
		self.instruction = self.memory.get(self.get_memory_address)


	def execute(self, instruction):
		if instruction == 'end':		# Se puede usar un switch
			self.scheduler.get_next
		else:
			if instruction == 'io':
				self.scheduler.get_next
				"instruction se va a IO"
		
		print instruction


	def get_memory_address(self):
		return self.base_address + self.pc
